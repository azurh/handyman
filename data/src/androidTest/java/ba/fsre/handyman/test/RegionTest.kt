package ba.fsre.handyman.test

import ba.fsre.handyman.domain.model.Region
import ba.fsre.handyman.domain.repository.RegionRepository
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.test.inject
import java.util.*

class RegionTest : BaseTest() {

    private val regionRepo: RegionRepository by inject()

    @Test
    fun testRegions() {
        runBlocking {
            val allRegions1 = regionRepo.getAll()

            val expectedRegion = Region(
                name = UUID.randomUUID().toString(),
                countryId = UUID.randomUUID().toString(),
            )

            assert(expectedRegion.id == null)

            val actualRegion = regionRepo.create(expectedRegion)
            assert(actualRegion.id != null)

            assert(expectedRegion.name == actualRegion.name)
            assert(expectedRegion.countryId == actualRegion.countryId)

            val allRegions2 = regionRepo.getAll()
            assert(allRegions1.size == allRegions2.size - 1)

            assert(expectedRegion.name == allRegions2.last().name)
            assert(expectedRegion.countryId == allRegions2.last().countryId)

            val detailService = regionRepo.getById(actualRegion.id!!)

            assert(actualRegion.id == detailService.id)
            assert(actualRegion.name == detailService.name)
            assert(actualRegion.countryId == detailService.countryId)

            regionRepo.delete(actualRegion.id!!)

            assert(allRegions1.size == regionRepo.getAll().size)
        }
    }

}

package ba.fsre.handyman.test

import ba.fsre.handyman.domain.model.Service
import ba.fsre.handyman.domain.repository.ServiceRepository
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.test.inject

class ServicesTest : BaseTest() {

    private val servicesRepo: ServiceRepository by inject()

    @Test
    fun testServices() {
        runBlocking {
            val allServices1 = servicesRepo.getAll()

            val expectedService = Service(
                name = uuid(),
                description = uuid(),
                categoryId = uuid(),
            )

            assert(expectedService.id == null)

            val actualService = servicesRepo.create(expectedService)
            assert(actualService.id != null)

            assert(expectedService.name == actualService.name)
            assert(expectedService.description == actualService.description)
            assert(expectedService.categoryId == actualService.categoryId)

            val allServices2 = servicesRepo.getAll()
            assert(allServices1.size == allServices2.size - 1)

            assert(expectedService.name == allServices2.last().name)
            assert(expectedService.description == allServices2.last().description)
            assert(expectedService.categoryId == allServices2.last().categoryId)

            val detailService = servicesRepo.getById(actualService.id!!)

            assert(actualService.id == detailService.id)
            assert(actualService.name == detailService.name)
            assert(actualService.description == detailService.description)
            assert(actualService.categoryId == detailService.categoryId)

            servicesRepo.delete(actualService.id!!)

            assert(allServices1.size == servicesRepo.getAll().size)

        }
    }

}

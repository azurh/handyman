package ba.fsre.handyman.test

import ba.fsre.handyman.domain.model.HandyHttpException
import ba.fsre.handyman.domain.model.User
import ba.fsre.handyman.domain.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.fail
import org.junit.Test
import org.koin.test.inject
import java.io.IOException
import java.util.*

class UsersTest : BaseTest() {

    private val userRepo: UserRepository by inject()

    @Test
    fun getAll() {
        runBlocking {
            val all = userRepo.getAllUsers()
            all.forEach {
                assert(it.cityName != null)
            }
        }
    }

    @Test
    fun testInvalidCity(): Unit {
        runBlocking {
            val inexistingCityId = uuid()

            val expectedUser = User(
                email = uuid(),
                firstName = uuid(),
                lastName = uuid(),
                password = uuid(),
                cityId = inexistingCityId
            )

            try {
                userRepo.register(expectedUser)
            } catch (e: IOException) {
                e.printStackTrace()
                if (e is HandyHttpException) {
                    assert(e.statusCode == 404)
                } else {
                    fail()
                }
            }

        }
    }

    @Test
    fun deleteUsers() {
        runBlocking {
            userRepo.getAllUsers().forEach {
                if (it.firstName!!.contains("azur", ignoreCase = true)) {
                    userRepo.delete(it.id!!)
                }
                try {
                    UUID.fromString(it.firstName!!)
                    userRepo.delete(it.id!!)
                } catch (e: Exception) {
                }
            }
        }
    }

}
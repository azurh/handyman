package ba.fsre.handyman.test

import ba.fsre.handyman.domain.model.Country
import ba.fsre.handyman.domain.repository.CountryRepository
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.test.inject

class CountryTest : BaseTest() {

    private val countryRepo: CountryRepository by inject()

    @Test
    fun countriesTest() {
        runBlocking {
            val expectedCountry = Country(
                name = uuid(),
                code = uuid()
            )

            assertNull(expectedCountry.id)

            val initialList = countryRepo.getAll()

            val actualCountry = countryRepo.createNew(expectedCountry)
            assertNotNull(actualCountry.id)

            assertEquals(expectedCountry.name, actualCountry.name)
            assertEquals(expectedCountry.code, actualCountry.code)

            val modifiedList = countryRepo.getAll()

            assertTrue(modifiedList.size - initialList.size == 1)

            actualCountry.name = uuid()
            countryRepo.updateExisting(actualCountry)

            val updatedCountry = countryRepo.getById(actualCountry.id!!)

            assertEquals(actualCountry.name, updatedCountry.name)
        }
    }


}

package ba.fsre.handyman.test

import androidx.test.platform.app.InstrumentationRegistry
import ba.fsre.handyman.di.DataDeps
import ba.fsre.handyman.domain.repository.AuthRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import java.util.*

abstract class BaseTest : KoinTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val session: SessionRepository by inject()
    private val auth: AuthRepository by inject()

    @Before
    fun onBefore() {
        Dispatchers.setMain(mainThreadSurrogate)
        startKoin {
            modules(DataDeps.modules + module {
                factory(named("appcontext")) {
                    InstrumentationRegistry.getInstrumentation().targetContext
                }
            })
        }
        runBlocking {
            session.obtainToken()
            auth.login("azur.haljeta@gmail.com", "12345678")
            session.obtainToken()
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    companion object {
        const val TAG = "BaseTest"
    }
}

fun BaseTest.uuid() = UUID.randomUUID().toString()
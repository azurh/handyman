package ba.fsre.handyman.test

import ba.fsre.handyman.domain.model.Category
import ba.fsre.handyman.domain.repository.CategoryRepository
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertFalse
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.test.inject
import java.util.*

class CityTest : BaseTest() {

    private val serviceCategoryRepository: CategoryRepository by inject()

    @Test
    fun serviceCategoriesTest() {
        runBlocking {

            // create local ServiceCategory
            val serviceCategory = Category(
                name = UUID.randomUUID().toString(),
                description = UUID.randomUUID().toString()
            )

            // create it remotely as well
            val addedServiceCategory = serviceCategoryRepository.addNew(serviceCategory)

            // check if the remote ServiceCategory matches the remote one
            assertEquals(serviceCategory.name, addedServiceCategory.name)
            assertEquals(serviceCategory.description, addedServiceCategory.description)

            // query the API for all ServiceCategories
            val all = serviceCategoryRepository.getAll()

            // get the last one
            val last = all.last()

            // compare if the last one matches the locally created ServiceCategory
            assertEquals(last.id, addedServiceCategory.id)
            assertEquals(last.name, addedServiceCategory.name)
            assertEquals(last.description, addedServiceCategory.description)

            assertEquals(last, addedServiceCategory)

            serviceCategoryRepository.delete(last.id!!)

            val all2 = serviceCategoryRepository.getAll()

            assertFalse(all2.last() == all.last())
            assert(all2.size == all.size - 1)
        }
    }
}

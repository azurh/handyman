package ba.fsre.handyman.test

import android.util.Log
import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import ba.fsre.handyman.util.date_utils.toIso8601
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.test.inject
import java.util.*

class FoundationTest : BaseTest() {

    private val requestsRepo: RequestRepository by inject()
    private val sessionRepo: SessionRepository by inject()

    @Test
    fun deleteAllOpenServiceRequests(): Unit = runBlocking {
        var page = 0
        var pagingBundle: PagingBundle<Request>?
        val list = mutableListOf<Request>()
        do {
            pagingBundle = requestsRepo.getAllOpenRequests(page)
            page++
            list.addAll(pagingBundle.list)
        } while (pagingBundle!!.list.isNotEmpty())

        list.forEach {
            try {
                requestsRepo.delete(it.id!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @Test
    fun deleteAllAcceptedJobs(): Unit = runBlocking {
        var page = 0
        var pagingBundle: PagingBundle<Request>?
        val list = mutableListOf<Request>()
        do {
            pagingBundle = requestsRepo.getUserJobs(sessionRepo.obtainUserId()!!, page)
            page++
            list.addAll(pagingBundle.list)
        } while (pagingBundle!!.list.isNotEmpty())

        list.forEach {
            try {
                requestsRepo.delete(it.id!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @Test
    fun deleteMyRequests(): Unit = runBlocking {
        var page = 0
        var pagingBundle: PagingBundle<Request>?
        val list = mutableListOf<Request>()
        do {
            pagingBundle = requestsRepo.getByUser(sessionRepo.obtainUserId()!!, page)
            page++
            list.addAll(pagingBundle.list)
        } while (pagingBundle!!.list.isNotEmpty())

        list.forEach {
            try {
                requestsRepo.delete(it.id!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @Test
    fun addPredefinedServiceRequests(): Unit = runBlocking {
        (azurSet1() + amarSet1()).shuffled().forEach {
            requestsRepo.create(it)
        }
    }

    @Test
    fun addPredefinedServiceRequests_1(): Unit = runBlocking {
        (amarSet1()).random().let {
            requestsRepo.create(it)
        }
    }

    companion object {
        private val TAG = "FoundationTest"

        enum class KnownServices(val id: String) {
            vodoinstalater("5fe894e70f2032c71c4a5818"),
            elektricar("5fe8951d0f2032c71c4a5819"),
            keramicar("5fe895d90f2032c71c4a581a"),
            bravar("5fe895e30f2032c71c4a581b"),
            stolar("5fe895eb0f2032c71c4a581c"),
            dimnjacar("5fe896080f2032c71c4a581d"),
            moler("5fe896200f2032c71c4a581e"),
            ciscenje("5fe896a40f2032c71c4a581f")
        }

        enum class KnownUsers(val id: String) {
            azur("6091a4002b322d00049f7aea"),
            amar("60566ee9e226aaa9d6890b86")
        }

        private fun randomDateInFuture(): Date {
            return Calendar.getInstance().apply {
                add(Calendar.DATE, (3..30).random())
            }.time.also {
                Log.i(TAG, "randomDateInFuture: ${it.toIso8601()}")
            }
        }

        private fun azurSet1(): List<Request> {
            return listOf(
                Request.createNew(
                    title = "Promjena sifona",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.vodoinstalater.id,
                    creatorId = KnownUsers.azur.id,
                    price = 30.0,
                ),
                Request.createNew(
                    title = "Krecenje hodnika",
                    description = "Lijevi zid u boji dinje, a desni u zelenu pantone.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.moler.id,
                    creatorId = KnownUsers.azur.id,
                    price = 45.0
                ),
                Request.createNew(
                    title = "Ulazna vrata slabo dihtuju",
                    description = "Gornji desni cosak nije stegnut.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.bravar.id,
                    creatorId = KnownUsers.azur.id,
                ),
                Request.createNew(
                    title = "Ocistiti dimnjak",
                    description = "",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.dimnjacar.id,
                    creatorId = KnownUsers.azur.id,
                    price = 100.0
                ),
                Request.createNew(
                    title = "Zategnuti vrata od ormara",
                    description = "Vrata skripe, treba ih zategnuti i podmazati.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.stolar.id,
                    creatorId = KnownUsers.azur.id,
                )
            )
        }

        private fun amarSet1(): List<Request> {
            return listOf(
                Request.createNew(
                    title = "Razvesti struju",
                    description = "Potrebno razvesti struju u cijelom prizemlju, u svakoj prostoriji. Uz to je potrebno dodati uticnice na par mjesta.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.elektricar.id,
                    creatorId = KnownUsers.amar.id,
                    price = 200.0
                ),

                Request.createNew(
                    title = "Promjena plocica",
                    description = "Plocice u WC-u su pocele opadati, potrebna osoba koja ce kvalitetno ugraditi nove plocice. Plocice su vec kupljene i sav dodatni materijal je spreman.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.keramicar.id,
                    creatorId = KnownUsers.amar.id
                ),

                Request.createNew(
                    title = "Potrebna ograda za balkon",
                    description = "Potreban mi je bravar koji ce napraviti kvalitetnu ogradu za balkon. Ogradu treba ofarbati u bijelu boju.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.bravar.id,
                    creatorId = KnownUsers.amar.id,
                    price = 50.0
                ),

                Request.createNew(
                    title = "Napraviti radni stol",
                    description = "Potreban mi je radni stol za posla, dimenzija 120x80x90. Radni stol treba da bude cvrst je ce na njemu stajati vise monitora te laptop i dodatna oprema.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.stolar.id,
                    creatorId = KnownUsers.amar.id,
                    price = 120.0
                ),

                Request.createNew(
                    title = "Ciscenje odzaka",
                    description = "Potrebno mi je ocistiti odzak u cijeloj zgradi jer imam problema sa dimom. Dim se vraca u .id,prostoriju u kojoj se nalazi sporet.",
                    startDate = randomDateInFuture().toIso8601(),
                    serviceId = KnownServices.dimnjacar.id,
                    creatorId = KnownUsers.amar.id,
                    price = 60.0
                ),
            )
        }
    }
}

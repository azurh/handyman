package ba.fsre.handyman.test

import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.ServiceRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import ba.fsre.handyman.util.date_utils.toIso8601
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.koin.test.inject
import java.util.*

class RequestsTest : BaseTest() {

    private val servicesRepo: ServiceRepository by inject()
    private val requestRepo: RequestRepository by inject()
    private val sessionRepo: SessionRepository by inject()

    @Test
    fun deleteAllMine(): Unit = runBlocking {
        val allMineAction =
            suspend { getRequestsExhaustivelyByUserId(sessionRepo.obtainUserId()!!) }
        allMineAction().forEach { requestRepo.delete(it.id!!) }
        assert(allMineAction.invoke().isEmpty())
    }

    @Test
    fun testFullRequestLifecycle(): Unit {
        runBlocking {

            val serviceList = servicesRepo.getAll()

            val request = requestRepo.create(
                Request.createNew(
                    title = uuid(),
                    description = uuid(),
                    startDate = Date().toIso8601(),
                    serviceId = serviceList.random().id!!,
                    creatorId = sessionRepo.obtainUserId()!!
                )
            )

            val expectedRequestsFromUser = requestRepo.getByUser(
                userId = sessionRepo.obtainUserId()!!
            ).totalDocs
            val allByUser = getRequestsExhaustivelyByUserId(sessionRepo.obtainUserId()!!)

            Assert.assertEquals(expectedRequestsFromUser, allByUser.size)
            Assert.assertNotNull(allByUser.find { it.id == request.id })

            val expectedRequestsTotal = requestRepo.getAllOpenRequests().totalDocs
            val all = getAllRequestsExhaustively()

            Assert.assertEquals(expectedRequestsTotal, all.size)

            requestRepo.delete(requestId = request.id!!)
        }
    }

    @Test
    fun makeRequestAccept(): Unit = runBlocking {
        val allRequests = suspend { getAllRequestsExhaustively() }

        val request = requestRepo.create(
            Request.createNew(
                title = uuid(),
                description = uuid(),
                startDate = Date().toIso8601(),
                serviceId = servicesRepo.getAll().random().id!!,
                creatorId = sessionRepo.obtainUserId()!!
            )
        )

        Assert.assertNotNull(request.id)
        Assert.assertNotNull(allRequests.invoke().find { it.id == request.id })
        Assert.assertNull(request.servicerId)

        val acceptedRequest = requestRepo.accept(
            requestId = request.id!!,
            userId = sessionRepo.obtainUserId()!!
        )
        Assert.assertNotNull(acceptedRequest.servicerId)
        assert(sessionRepo.obtainUserId()!! == acceptedRequest.servicerId)

        Assert.assertNull(allRequests.invoke().find { it.id == request.id })
    }

    @Test
    fun testMyJobs(): Unit = runBlocking {
        val request = requestRepo.create(
            Request.createNew(
                title = uuid(),
                description = uuid(),
                startDate = Date().toIso8601(),
                serviceId = servicesRepo.getAll().random().id!!,
                creatorId = sessionRepo.obtainUserId()!!
            )
        )
        val acceptedRequest = requestRepo.accept(request.id!!, sessionRepo.obtainUserId()!!)

        val userJobsFetcher = suspend {
            val list = mutableListOf<Request>()
            var page = 0
            var response: PagingBundle<Request>?
            do {
                response = requestRepo.getUserJobs(sessionRepo.obtainUserId()!!, page)
                list.addAll(response.list)
                page++
            } while (response!!.list.isNotEmpty())
            list
        }

        Assert.assertNotNull(userJobsFetcher.invoke().find { it.id == acceptedRequest.id })
    }

    @Test
    fun markAsCompleted(): Unit = runBlocking {
        val request = requestRepo.create(
            Request.createNew(
                title = uuid(),
                description = uuid(),
                startDate = Date().toIso8601(),
                serviceId = servicesRepo.getAll().random().id!!,
                creatorId = sessionRepo.obtainUserId()!!
            )
        )

        getAllRequestsExhaustively().find { it.id == request.id }.let {
            Assert.assertNotNull(it)
            Assert.assertFalse(it!!.completed)
        }

        requestRepo.accept(
            requestId = request.id!!,
            userId = sessionRepo.obtainUserId()!!
        )

        val completedRequest = requestRepo.markAsCompleted(
            requestId = request.id!!,
            userId = sessionRepo.obtainUserId()!!
        )

        Assert.assertEquals(request.id!!, completedRequest.id!!)
    }

    private suspend fun getRequestsExhaustivelyByUserId(userId: String): List<Request> {

        suspend fun getByPage(id: String, page: Int): PagingBundle<Request> {
            return requestRepo.getByUser(id, page)
        }

        var page = 0
        val list = mutableListOf<Request>()

        var response: PagingBundle<Request>?
        do {
            response = getByPage(userId, page)
            list.addAll(response.list)
            page = response.nextPage
        } while (response!!.list.isNotEmpty())

        return list
    }

    private suspend fun getAllRequestsExhaustively(): List<Request> {

        suspend fun getByPage(page: Int): PagingBundle<Request> {
            return requestRepo.getAllOpenRequests(page)
        }

        var page = 0
        val list = mutableListOf<Request>()

        var response: PagingBundle<Request>?
        do {
            response = getByPage(page)
            list.addAll(response.list)
            page = response.nextPage
        } while (response!!.list.isNotEmpty())

        return list
    }

}

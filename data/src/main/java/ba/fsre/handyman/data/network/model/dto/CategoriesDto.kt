package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import com.google.gson.annotations.SerializedName

class CategoriesDto {

    class Dto(
        @SerializedName("_id")
        val id: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("__v")
        val v: String
    )

    class SingleResponse(data: Dto) : NetworkResponse<Dto>(data)
    class GetAllResponse(data: List<Dto>) : NetworkResponse<List<Dto>>(data)

    class CreateNewRequestBody(
        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String
    )
}
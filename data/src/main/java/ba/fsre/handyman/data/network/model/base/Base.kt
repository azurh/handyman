package ba.fsre.handyman.data.network.model.base

import com.google.gson.annotations.SerializedName

abstract class NetworkResponse<D>(
    @SerializedName("data")
    var data: D
) {
    @SerializedName("message")
    lateinit var message: String

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("code")
    var code: Int = -1
}

class ErrorResponse(val message: String)
package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import ba.fsre.handyman.domain.model.User
import com.google.gson.annotations.SerializedName

class UsersDto {

    companion object {
        fun dtoToUser(dto: Dto): User {
            return User(
                email = dto.email,
                id = dto.id,
                firstName = dto.firstName,
                lastName = dto.lastName,
                address = dto.address,
                phone = dto.phoneNumber,
                cityId = dto.cityId,
                cityName = dto.cityName,
                services = dto.services,
                fcmTokens = dto.fcmTokens
            )
        }
    }

    class Dto(
        @SerializedName("_id")
        val id: String? = null,

        @SerializedName("first_name")
        val firstName: String? = null,

        @SerializedName("last_name")
        val lastName: String? = null,

        @SerializedName("email")
        val email: String,

        @SerializedName("password")
        val password: String? = null,

        @SerializedName("address")
        val address: String? = null,

        @SerializedName("phone_number")
        val phoneNumber: String? = null,

        @SerializedName("services")
        val services: List<String>? = null,

        @SerializedName("city_id")
        val cityId: String,

        @SerializedName("city_name")
        val cityName: String,

        @SerializedName("fcm_tokens")
        val fcmTokens: List<String>? = null
    )

    class ListResponse(data: List<Dto>) : NetworkResponse<List<Dto>>(data)

    class SingleResponse(data: Dto) : NetworkResponse<Dto>(data)

    class RegistrationRequest(
        @SerializedName("first_name")
        val firstName: String,

        @SerializedName("last_name")
        val lastName: String,

        @SerializedName("email")
        val email: String,

        @SerializedName("password")
        val password: String,

        @SerializedName("city_id")
        val cityId: String,

        @SerializedName("address")
        val address: String? = null,

        @SerializedName("phone_number")
        val phoneNumber: String? = null
    )

    class AddFcmTokenRequest(
        @SerializedName("user_id")
        val userId: String,

        @SerializedName("fcm_token")
        val fcmToken: String
    )

    class GetByIdRequest(
        @SerializedName("user_id")
        val userId: String
    )

    class UpdateUserServicesRequest(
        @SerializedName("user_id")
        val userId: String,
        @SerializedName("services")
        val services: List<String>
    )
}
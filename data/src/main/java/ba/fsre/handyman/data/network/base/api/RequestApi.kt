package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.RequestsDto
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface RequestApi {

    @GET("/api/v1/service-requests/")
    suspend fun getAll(
        @Query("page") page: Int,
        @Query("size") size: Int = 10,
        @Query("id") userId: String? = null
    ): RequestsDto.GetAllResponse

    /*@POST("/api/v1/service-requests/")
    suspend fun create(@Body requestBody: RequestsDto.CreateNewRequestBody): RequestsDto.SingleResponse*/

    @Multipart
    @POST("/api/v1/service-requests/")
    suspend fun create(
        @Part("title") title: RequestBody,
        @Part("description") description: RequestBody?,
        @Part("start_date") startDate: RequestBody,
        @Part("price") price: RequestBody?,
        @Part("service_id") serviceId: RequestBody,
        @Part("creator_id") creatorId: RequestBody,
        @Part image: MultipartBody.Part? = null
    ): RequestsDto.SingleResponse

    @GET("/api/v1/service-requests/{userId}")
    suspend fun getAllByUser(
        @Path("userId") userId: String,
        @Query("page") page: Int,
        @Query("size") size: Int = 10
    ): RequestsDto.GetAllResponse

    @GET("/api/v1/service-requests/{userId}/jobs")
    suspend fun getUserJobs(
        @Path("userId") userId: String,
        @Query("page") page: Int,
        @Query("size") size: Int = 10
    ): RequestsDto.GetAllResponse

    @POST("/api/v1/service-requests/complete")
    suspend fun markAsCompleted(
        @Body requestBody: RequestsDto.MarkAsCompletedRequestBody
    ): RequestsDto.SingleResponse

    @POST("/api/v1/service-requests/accept")
    suspend fun accept(
        @Body requestBody: RequestsDto.AcceptRequestRequestBody
    ): RequestsDto.SingleResponse

    @DELETE("/api/v1/service-requests/{id}")
    suspend fun deleteRequest(@Path("id") id: String)

}
package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import com.google.gson.annotations.SerializedName

class RequestsDto {

    class Dto(
        @SerializedName("servicer_id")
        val servicerId: String? = null,

        @SerializedName("completed")
        val completed: Boolean,

        @SerializedName("feedback_left")
        val feedbackLeft: Boolean,

        @SerializedName("updated_at")
        val updatedAt: String,

        @SerializedName("price")
        val price: Double?,

        @SerializedName("_id")
        val id: String,

        @SerializedName("title")
        val title: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("start_date")
        val startDate: String,

        @SerializedName("due_date")
        val dueDate: String,

        @SerializedName("service_id")
        val serviceId: String,

        @SerializedName("creator_id")
        val creatorId: String,

        @SerializedName("created_at")
        val createdAt: String,

        @SerializedName("__v")
        val v: Int,

        @SerializedName("service_category_id")
        val serviceCategoryId: String,

        @SerializedName("image")
        val image: String?
    )

    class PagedResponse(
        @SerializedName("docs")
        val docs: List<Dto>,

        @SerializedName("totalDocs")
        val totalDocs: Int,

        @SerializedName("page")
        val page: Int,

        @SerializedName("nextPage")
        val nextPage: Int
    )

    /*class CreateNewRequestBody(
        @SerializedName("title")
        val title: String,

        @SerializedName("description")
        val description: String?,

        @SerializedName("start_date")
        val startDate: String,

        @SerializedName("price")
        val price: Double?,

        @SerializedName("service_id")
        val serviceId: String,

        @SerializedName("creator_id")
        val creatorId: String,
    )*/

    class MarkAsCompletedRequestBody(
        @SerializedName("service_request_id")
        val serviceRequestId: String,

        @SerializedName("user_id")
        val userId: String
    )

    class AcceptRequestRequestBody(
        @SerializedName("service_request_id")
        val serviceRequestId: String,

        @SerializedName("user_id")
        val userId: String
    )

    class SingleResponse(data: Dto) : NetworkResponse<Dto>(data)
    class GetAllResponse(data: PagedResponse) : NetworkResponse<PagedResponse>(data)
}
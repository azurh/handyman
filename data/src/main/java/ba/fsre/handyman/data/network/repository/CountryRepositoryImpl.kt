package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.CountriesApi
import ba.fsre.handyman.data.network.model.dto.CountriesDto
import ba.fsre.handyman.domain.model.Country
import ba.fsre.handyman.domain.repository.CountryRepository

class CountryRepositoryImpl(private val apiService: CountriesApi) : CountryRepository {

    override suspend fun getAll(): List<Country> {
        return apiService.getAllCountries().data.map {
            Country(it.id, it.name, it.code)
        }
    }

    override suspend fun getById(id: String): Country {
        return apiService.getCountryById(id).data.let {
            Country(it.id, it.name, it.code)
        }
    }

    override suspend fun createNew(country: Country): Country {
        return apiService.addCountry(
            CountriesDto.AddCountryRequest(
                name = country.name,
                code = country.code
            )
        ).data.let {
            Country(
                id = it.id,
                name = it.name,
                code = it.code
            )
        }
    }

    override suspend fun updateExisting(country: Country) {
        apiService.updateCountry(
            countryId = country.id!!,
            requestBody = CountriesDto.UpdateCountryRequest(
                name = country.name,
                code = country.code
            )
        )
    }

    override suspend fun delete(id: String) {
        apiService.deleteCountry(id)
    }


}
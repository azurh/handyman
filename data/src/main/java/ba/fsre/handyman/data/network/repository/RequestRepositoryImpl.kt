package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.RequestApi
import ba.fsre.handyman.data.network.model.dto.RequestsDto
import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.model.Service
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.ServiceRepository
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class RequestRepositoryImpl(val api: RequestApi, val serviceRepo: ServiceRepository) :
    RequestRepository {

    var availableServices: List<Service> = emptyList()

    override suspend fun getAllOpenRequests(page: Int, userId: String?): PagingBundle<Request> {
        val all = api.getAll(page, userId = userId)
        val list = all.data.docs.map {
            mapToDomain(it)
        }
        return PagingBundle(list, all.data.page, all.data.totalDocs)
    }

    override suspend fun create(request: Request, image: File?): Request {
        val convert: (Any?) -> RequestBody? = {
            if (it == null) {
                null
            } else {
                it.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            }
        }

        val response = api.create(
            title = convert(request.title)!!,
            description = convert(request.description),
            startDate = convert(request.startDate)!!,
            price = convert(request.price),
            serviceId = convert(request.serviceId)!!,
            creatorId = convert(request.creatorId)!!,
            image = kotlin.run {
                if (image == null)
                    null
                else {
                    MultipartBody.Part.createFormData(
                        "image",
                        image.name,
                        image.asRequestBody("image/*".toMediaTypeOrNull())
                    )
                }
            }
        )
        return mapToDomain(response.data)
    }

    override suspend fun getByUser(userId: String, page: Int): PagingBundle<Request> {
        val all = api.getAllByUser(userId, page)
        val list = all.data.docs.map {
            mapToDomain(it)
        }
        return PagingBundle(list, all.data.page, all.data.totalDocs)
    }

    override suspend fun markAsCompleted(requestId: String, userId: String): Request {
        val response = api.markAsCompleted(
            RequestsDto.MarkAsCompletedRequestBody(
                serviceRequestId = requestId,
                userId = userId
            )
        )
        return mapToDomain(response.data)
    }

    override suspend fun accept(requestId: String, userId: String): Request {
        val response = api.accept(
            RequestsDto.AcceptRequestRequestBody(
                serviceRequestId = requestId,
                userId = userId
            )
        )
        return mapToDomain(response.data)
    }

    override suspend fun getUserJobs(userId: String, page: Int): PagingBundle<Request> {
        val all = api.getUserJobs(userId, page)
        val list = all.data.docs.map {
            mapToDomain(it)
        }
        return PagingBundle(list, all.data.page, all.data.totalDocs)

    }

    override suspend fun delete(requestId: String) {
        api.deleteRequest(requestId)
    }

    private suspend fun mapToDomain(singleDto: RequestsDto.Dto): Request {
        if (availableServices.isEmpty()) {
            availableServices = serviceRepo.getAll()
        }
        return Request(
            id = singleDto.id,
            title = singleDto.title,
            description = singleDto.description,
            completed = singleDto.completed,
            feedbackLeft = singleDto.feedbackLeft,
            serviceId = singleDto.serviceId,
            creatorId = singleDto.creatorId,
            createdAt = singleDto.createdAt,
            servicerId = singleDto.servicerId,
            updatedAt = singleDto.updatedAt,
            price = singleDto.price,
            startDate = singleDto.startDate,
            serviceName = availableServices.find { it.id == singleDto.serviceId }!!.name,
            image = if (singleDto.image != null) {
                "https://sum-ep-grupa7.herokuapp.com${singleDto.image}"
            } else null
        )
    }
}
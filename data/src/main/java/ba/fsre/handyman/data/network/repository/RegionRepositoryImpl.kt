package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.RegionApi
import ba.fsre.handyman.data.network.model.dto.RegionsDto
import ba.fsre.handyman.domain.model.Region
import ba.fsre.handyman.domain.repository.RegionRepository

class RegionRepositoryImpl(val api: RegionApi) : RegionRepository {

    override suspend fun getAll(): List<Region> {
        return api.getAll().data.map {
            Region(it.id, it.name, it.countryId)
        }
    }

    override suspend fun getById(id: String): Region {
        return api.getSingle(id).data.let {
            Region(it.id, it.name, it.countryId)
        }
    }

    override suspend fun create(region: Region): Region {
        return api.create(
            RegionsDto.CreateNewRequest(
                name = region.name,
                countryId = region.countryId
            )
        ).data
            .let {
                Region(
                    id = it.id,
                    name = it.name,
                    countryId = it.countryId
                )
            }
    }

    override suspend fun delete(id: String) {
        api.delete(id)
    }
}
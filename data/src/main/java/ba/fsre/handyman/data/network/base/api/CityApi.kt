package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.CitiesDto
import retrofit2.http.*

interface CityApi {

    @GET("/api/v1/cities")
    suspend fun getAllCities(): CitiesDto.ListResponse

    @GET("/api/v1/cities/{cityId}")
    suspend fun getCityById(@Path("cityId") cityId: String): CitiesDto.SingleResponse

    @POST("/api/v1/cities")
    suspend fun addCity(@Body addCityRequest: CitiesDto.AddCityRequest): CitiesDto.SingleResponse

    @PUT("/api/v1/cities/{cityId}")
    suspend fun updateCity(
        @Path("cityId") cityId: String,
        @Body updateCityRequest: CitiesDto.UpdateCityRequest
    ): CitiesDto.SingleResponse

    @DELETE("/api/v1/cities/{id}")
    suspend fun delete(@Path("id") id: String)
}
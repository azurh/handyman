package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.UsersDto
import retrofit2.http.*

interface UserApi {

    @POST("/api/v1/users/register")
    suspend fun register(@Body requestBody: UsersDto.RegistrationRequest): UsersDto.SingleResponse

    @GET("/api/v1/users")
    suspend fun getAllUsers(): UsersDto.ListResponse

    @GET("/api/v1/users/details/{id}")
    suspend fun getUserById(@Path("id") id: String): UsersDto.SingleResponse

    @PUT("/api/v1/users/add-fcm-token")
    suspend fun addFcmToken(@Body requestBody: UsersDto.AddFcmTokenRequest)

    @PUT("/api/v1/users/update-services")
    suspend fun updateUserServices(@Body requestBody: UsersDto.UpdateUserServicesRequest)

    @DELETE("/api/v1/users/{id}")
    suspend fun deleteUser(@Path("id") id: String)
}
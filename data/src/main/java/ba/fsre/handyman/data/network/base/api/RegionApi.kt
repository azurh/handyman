package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.RegionsDto
import retrofit2.http.*

interface RegionApi {

    @GET("/api/v1/regions")
    suspend fun getAll(): RegionsDto.ListResponse

    @GET("/api/v1/regions/{id}")
    suspend fun getSingle(@Path("id") id: String): RegionsDto.SingleResponse

    @POST("/api/v1/regions")
    suspend fun create(@Body requestBody: RegionsDto.CreateNewRequest): RegionsDto.SingleResponse

    @DELETE("/api/v1/regions/{id}")
    suspend fun delete(@Path("id") id: String)
}
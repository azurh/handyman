package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.AuthDto
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    @POST("/api/v1/users/login")
    suspend fun login(@Body requestBody: AuthDto.LoginRequest): AuthDto.LoginResponse
}
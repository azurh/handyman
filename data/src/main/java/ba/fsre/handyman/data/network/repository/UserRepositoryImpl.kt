package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.UserApi
import ba.fsre.handyman.data.network.model.dto.UsersDto
import ba.fsre.handyman.domain.model.User
import ba.fsre.handyman.domain.repository.UserRepository

class UserRepositoryImpl(private val userService: UserApi) : UserRepository {

    override suspend fun register(user: User) {
        userService.register(
            UsersDto.RegistrationRequest(
                firstName = user.firstName!!,
                lastName = user.lastName!!,
                email = user.email,
                password = user.password!!,
                cityId = user.cityId,
                address = user.address,
                phoneNumber = user.phone,
            )
        )
    }

    override suspend fun getAllUsers(): List<User> {
        return userService.getAllUsers().data
            .map {
                UsersDto.dtoToUser(it)
            }
    }

    override suspend fun getUserById(userId: String): User {
        return userService.getUserById(userId).data
            .let {
                UsersDto.dtoToUser(it)
            }
    }

    override suspend fun addFcmToken(userId: String, fcmToken: String) {
        userService.addFcmToken(UsersDto.AddFcmTokenRequest(userId, fcmToken))
    }

    override suspend fun updateUserServices(userId: String, services: List<String>) {
        userService.updateUserServices(UsersDto.UpdateUserServicesRequest(userId, services))
    }

    override suspend fun delete(userId: String) {
        userService.deleteUser(userId)
    }
}
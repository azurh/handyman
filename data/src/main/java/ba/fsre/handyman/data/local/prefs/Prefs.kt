package ba.fsre.handyman.data.local.prefs

import android.content.Context
import androidx.core.content.edit

internal class Prefs internal constructor(context: Context, name: String) : KeyValueStorage {

    private val sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    override fun putString(key: String, value: String) {
        sharedPreferences.edit {
            putString(key, value)
        }
    }

    override fun getString(key: String): String? {
        return sharedPreferences.getString(key, null)
    }

    override fun clearKey(key: String) {
        sharedPreferences.edit(commit = true) {
            remove(key)
        }
    }

}
package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.CategoriesDto
import retrofit2.http.*

interface CategoryApi {

    @GET("/api/v1/service-categories")
    suspend fun getAll(): CategoriesDto.GetAllResponse

    @GET("/api/v1/service-categories/{id}")
    suspend fun getSingle(@Path("id") id: String): CategoriesDto.SingleResponse

    @POST("/api/v1/service-categories")
    suspend fun create(@Body requestBody: CategoriesDto.CreateNewRequestBody): CategoriesDto.SingleResponse

    @DELETE("/api/v1/service-categories/{id}")
    suspend fun delete(@Path("id") id: String)
}
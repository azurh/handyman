package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import com.google.gson.annotations.SerializedName

class ServicesDto {

    class Dto(
        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String? = null,

        @SerializedName("__v")
        val v: Int,

        @SerializedName("service_category_id")
        val serviceCategoryId: String,

        @SerializedName("_id")
        val id: String
    )

    class SingleResponse(data: Dto) : NetworkResponse<Dto>(data)
    class GetAllResponse(data: List<Dto>) : NetworkResponse<List<Dto>>(data)

    class CreateNewRequestBody(
        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("service_category_id")
        val serviceCategoryId: String
    )
}
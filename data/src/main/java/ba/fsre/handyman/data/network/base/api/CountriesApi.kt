package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.CountriesDto
import retrofit2.http.*

interface CountriesApi {

    @GET("/api/v1/countries")
    suspend fun getAllCountries(): CountriesDto.ListResponse

    @GET("/api/v1/countries/{countryId}")
    suspend fun getCountryById(@Path("countryId") countryId: String): CountriesDto.SingleResponse

    @POST("/api/v1/countries")
    suspend fun addCountry(@Body requestBody: CountriesDto.AddCountryRequest): CountriesDto.SingleResponse

    @PUT("/api/v1/countries/{countryId}")
    suspend fun updateCountry(
        @Path("countryId") countryId: String,
        @Body requestBody: CountriesDto.UpdateCountryRequest
    )

    @DELETE("/api/v1/countries/{countryId}")
    suspend fun deleteCountry(@Path("countryId") countryId: String)

}
package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.CityApi
import ba.fsre.handyman.data.network.model.dto.CitiesDto
import ba.fsre.handyman.domain.model.City
import ba.fsre.handyman.domain.repository.CityRepository

class CityRepositoryImpl(private val cityService: CityApi) : CityRepository {

    override suspend fun getAllCities(): List<City> {
        return cityService.getAllCities().data.map {
            City(it.name, it.regionId).apply {
                this.id = it.id
            }
        }
    }

    override suspend fun getCityById(cityId: String): City {
        return cityService.getCityById(cityId).data.let {
            City(it.name, it.regionId)
        }
    }

    override suspend fun addCity(city: City): City {
        return cityService.addCity(
            CitiesDto.AddCityRequest(city.name, city.regionId)
        ).data.let {
            City(it.name, it.regionId)
        }
    }

    override suspend fun updateCity(city: City): Boolean {
        return cityService.updateCity(
            city.id, CitiesDto.UpdateCityRequest(city.name)
        ).run {
            true
        }
    }

    override suspend fun delete(id: String) {
        cityService.delete(id)
    }
}
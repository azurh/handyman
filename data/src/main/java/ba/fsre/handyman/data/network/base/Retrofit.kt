package ba.fsre.handyman.data.network.base

import ba.fsre.handyman.data.network.base.api.*
import ba.fsre.handyman.domain.repository.SessionRepository
import okhttp3.OkHttpClient
import okhttp3.internal.http2.ConnectionShutdownException
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

internal class Retrofit(url: String, sessionRepo: SessionRepository) {

    private val client = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        })
        .addInterceptor {
            val accessToken = sessionRepo.obtainToken()
            if (accessToken != null) {
                val request = it.request()
                    .newBuilder()
                    .addHeader(ACCESS_TOKEN_HEADER, sessionRepo.obtainToken()!!)
                    .build()
                it.proceed(request)
            } else {
                it.proceed(it.request())
            }
        }
        .addInterceptor {
            try {
                val request = it.request()
                val response = it.proceed(request)
                if (!response.isSuccessful) {
                    val msg = JSONObject(response.body!!.string()).getString("message")
                    throw Exception(msg)
                } else {
                    response
                }
            } catch (e: Exception) {
                val msg: String
                when (e) {
                    is SocketTimeoutException -> {
                        msg = "Timeout - Please check your internet connection"
                    }
                    is UnknownHostException -> {
                        msg = "Unable to make a connection. Please check your internet"
                    }
                    is ConnectionShutdownException -> {
                        msg = "Connection shutdown. Please check your internet"
                    }
                    is IllegalStateException -> {
                        msg = "${e.message}"
                    }
                    is HttpException -> {
                        msg = "${e.message}"
                    }
                    else -> {
                        msg = "${e.message}"
                    }
                }
                throw IOException(msg, e)
            }
        }
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    internal val countriesApi by lazy { retrofit.create(CountriesApi::class.java) }
    internal val authApi by lazy { retrofit.create(AuthApi::class.java) }
    internal val userApi by lazy { retrofit.create(UserApi::class.java) }
    internal val cityApi by lazy { retrofit.create(CityApi::class.java) }
    internal val categoriesApi by lazy { retrofit.create(CategoryApi::class.java) }
    internal val regionApi by lazy { retrofit.create(RegionApi::class.java) }
    internal val serviceApi by lazy { retrofit.create(ServiceApi::class.java) }
    internal val requestApi by lazy { retrofit.create(RequestApi::class.java) }

    companion object {
        const val TAG = "Retrofit"
        const val ACCESS_TOKEN_HEADER = "x-access-token"
    }

}
package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import com.google.gson.annotations.SerializedName

class AuthDto {

    class LoginRequest(
        @SerializedName("email")
        val email: String,

        @SerializedName("password")
        val password: String
    )

    class LoginResponse(data: Data) :
        NetworkResponse<LoginResponse.Data>(data) {

        class Data(
            @SerializedName("message")
            val message: String,

            @SerializedName("_id")
            val userId: String,

            @SerializedName("email")
            val email: String,

            @SerializedName("access_token")
            val accessToken: String,

            @SerializedName("refresh_token")
            val refreshToken: String
        )
    }

}
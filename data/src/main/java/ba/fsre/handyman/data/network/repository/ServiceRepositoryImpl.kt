package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.ServiceApi
import ba.fsre.handyman.data.network.model.dto.ServicesDto
import ba.fsre.handyman.domain.model.Service
import ba.fsre.handyman.domain.repository.ServiceRepository

class ServiceRepositoryImpl(val api: ServiceApi) : ServiceRepository {

    override suspend fun getAll(): List<Service> {
        return api.getAll().data.map {
            Service(
                id = it.id,
                name = it.name,
                description = it.description,
                categoryId = it.serviceCategoryId
            )
        }
    }

    override suspend fun getById(id: String): Service {
        return api.getSingle(id).data.let {
            Service(
                id = it.id,
                name = it.name,
                description = it.description,
                categoryId = it.serviceCategoryId
            )
        }
    }

    override suspend fun create(service: Service): Service {
        return api.create(
            ServicesDto.CreateNewRequestBody(
                name = service.name,
                description = service.description!!,
                serviceCategoryId = service.categoryId
            )
        ).data.let {
            Service(
                id = it.id,
                name = it.name,
                description = it.description,
                categoryId = it.serviceCategoryId
            )
        }
    }

    override suspend fun delete(id: String) {
        api.delete(id)
    }
}
package ba.fsre.handyman.data.local.prefs

interface KeyValueStorage {
    fun putString(key: String, value: String)
    fun getString(key: String): String?
    fun clearKey(key: String)
}
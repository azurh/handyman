package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import com.google.gson.annotations.SerializedName

class RegionsDto {

    class Dto(
        @SerializedName("_id")
        val id: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("country_id")
        val countryId: String,

        @SerializedName("__v")
        val v: String
    )

    class ListResponse(data: List<Dto>) : NetworkResponse<List<Dto>>(data)

    class SingleResponse(data: Dto) : NetworkResponse<Dto>(data)

    class CreateNewRequest(
        @SerializedName("name")
        val name: String,

        @SerializedName("country_id")
        val countryId: String
    )
}
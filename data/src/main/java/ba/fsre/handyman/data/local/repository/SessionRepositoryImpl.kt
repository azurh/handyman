package ba.fsre.handyman.data.local.repository

import ba.fsre.handyman.data.local.prefs.KeyValueStorage
import ba.fsre.handyman.domain.repository.SessionRepository

internal class SessionRepositoryImpl(
    private val keyValueStorage: KeyValueStorage
) : SessionRepository {

    override fun saveToken(accessToken: String, refreshToken: String) {
        keyValueStorage.putString(KEY_TOKEN, accessToken)
    }

    override fun obtainToken(): String? {
        return keyValueStorage.getString(KEY_TOKEN)
    }

    override fun saveUserId(userId: String) {
        keyValueStorage.putString(KEY_UID, userId)
    }

    override fun obtainUserId(): String? {
        return keyValueStorage.getString(KEY_UID)
    }

    override fun clear() {
        keyValueStorage.clearKey(KEY_TOKEN)
        keyValueStorage.clearKey(KEY_UID)
    }

    private companion object Constants {
        private const val KEY_TOKEN = "token"
        private const val KEY_UID = "uid"
    }
}
package ba.fsre.handyman.data.network.model.dto

import ba.fsre.handyman.data.network.model.base.NetworkResponse
import com.google.gson.annotations.SerializedName

class CitiesDto {

    class Dto(
        @SerializedName("_id")
        val id: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("region_id")
        val regionId: String,

        @SerializedName("__v")
        val v: String
    )

    class ListResponse(data: List<Dto>) : NetworkResponse<List<Dto>>(data)

    class SingleResponse(data: Dto) : NetworkResponse<Dto>(data)

    class AddCityRequest(
        @SerializedName("name")
        val name: String,

        @SerializedName("region_id")
        val regionId: String
    )

    class UpdateCityRequest(
        @SerializedName("name")
        val name: String
    )
}
package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.AuthApi
import ba.fsre.handyman.data.network.model.dto.AuthDto
import ba.fsre.handyman.domain.repository.AuthRepository
import ba.fsre.handyman.domain.repository.SessionRepository

class AuthRepositoryImpl(
    private val authApi: AuthApi,
    private val sessionRepo: SessionRepository
) : AuthRepository {

    override suspend fun login(email: String, password: String): String {
        return authApi.login(
            AuthDto.LoginRequest(
                email = email,
                password = password
            )
        ).also {
            sessionRepo.saveToken(it.data.accessToken, it.data.refreshToken)
            sessionRepo.saveUserId(it.data.userId)
        }.run {
            this.data.accessToken
        }
    }
}
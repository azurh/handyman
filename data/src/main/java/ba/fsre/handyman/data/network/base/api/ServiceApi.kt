package ba.fsre.handyman.data.network.base.api

import ba.fsre.handyman.data.network.model.dto.ServicesDto
import retrofit2.http.*

interface ServiceApi {

    @GET("/api/v1/services")
    suspend fun getAll(): ServicesDto.GetAllResponse

    @GET("/api/v1/services/{id}")
    suspend fun getSingle(@Path("id") id: String): ServicesDto.SingleResponse

    @POST("/api/v1/services")
    suspend fun create(@Body requestBody: ServicesDto.CreateNewRequestBody): ServicesDto.SingleResponse

    @DELETE("/api/v1/services/{id}")
    suspend fun delete(@Path("id") id: String)

}
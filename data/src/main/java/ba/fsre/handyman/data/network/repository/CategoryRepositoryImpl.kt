package ba.fsre.handyman.data.network.repository

import ba.fsre.handyman.data.network.base.api.CategoryApi
import ba.fsre.handyman.data.network.model.dto.CategoriesDto
import ba.fsre.handyman.domain.model.Category
import ba.fsre.handyman.domain.repository.CategoryRepository

class CategoryRepositoryImpl(private val api: CategoryApi) : CategoryRepository {

    override suspend fun getAll(): List<Category> {
        return api.getAll().data.map {
            Category(
                it.id,
                it.name,
                it.description
            )
        }
    }

    override suspend fun getSingle(id: String): Category {
        return api.getSingle(id).let {
            Category(
                it.data.id,
                it.data.name,
                it.data.description
            )
        }
    }

    override suspend fun addNew(category: Category): Category {
        return api.create(
            CategoriesDto.CreateNewRequestBody(
                name = category.name,
                description = category.description
            )
        ).let {
            Category(
                it.data.id,
                it.data.name,
                it.data.description
            )
        }
    }

    override suspend fun delete(id: String) {
        api.delete(id)
    }

}
package ba.fsre.handyman.di

import ba.fsre.handyman.data.local.prefs.KeyValueStorage
import ba.fsre.handyman.data.local.prefs.Prefs
import ba.fsre.handyman.data.local.repository.SessionRepositoryImpl
import ba.fsre.handyman.data.network.base.Retrofit
import ba.fsre.handyman.data.network.base.api.*
import ba.fsre.handyman.data.network.repository.*
import ba.fsre.handyman.di.DataDeps.Constants.PREFS_NAME
import ba.fsre.handyman.domain.repository.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

object DataDeps {

    private val httpModule = module {
        single {
            Retrofit(
                url = Constants.URL,
                sessionRepo = get()
            )
        }
        single<CountriesApi> {
            get<Retrofit>().countriesApi
        }
        single<AuthApi> {
            get<Retrofit>().authApi
        }
        single<UserApi> {
            get<Retrofit>().userApi
        }
        single<CityApi> {
            get<Retrofit>().cityApi
        }
        single<CategoryApi> {
            get<Retrofit>().categoriesApi
        }
        single<RegionApi> {
            get<Retrofit>().regionApi
        }
        single<ServiceApi> {
            get<Retrofit>().serviceApi
        }
        single<RequestApi> {
            get<Retrofit>().requestApi
        }
    }

    private val repositoryModule = module {
        factory<CountryRepository> {
            CountryRepositoryImpl(get())
        }
        factory<AuthRepository> {
            AuthRepositoryImpl(get(), get())
        }
        factory<SessionRepository> {
            SessionRepositoryImpl(get())
        }
        factory<UserRepository> {
            UserRepositoryImpl(get())
        }
        factory<CityRepository> {
            CityRepositoryImpl(get())
        }
        factory<CategoryRepository> {
            CategoryRepositoryImpl(get())
        }
        factory<RegionRepository> {
            RegionRepositoryImpl(get())
        }
        factory<ServiceRepository> {
            ServiceRepositoryImpl(get())
        }
        factory<RequestRepository> {
            RequestRepositoryImpl(get(), get())
        }
    }

    private val localModule = module {
        single<KeyValueStorage> { Prefs(get(named("appcontext")), PREFS_NAME) }
    }

    val cmodule = module {
        factory(named("appcontext")) {
            androidContext()
        }
    }

    val modules = httpModule + repositoryModule + localModule

    private object Constants {
        const val URL = "https://sum-ep-grupa7.herokuapp.com/"
        const val PREFS_NAME = "shared_prefs"
    }
}
package ba.fsre.handyman.util.date_utils

import java.text.SimpleDateFormat
import java.util.*

fun Date.toIso8601(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK)
    return sdf.format(this)
}

fun aa() {

}

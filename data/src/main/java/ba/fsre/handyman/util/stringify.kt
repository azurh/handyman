package ba.fsre.handyman.util

import com.google.gson.Gson

fun Any.stringify() = Gson().toJson(this)
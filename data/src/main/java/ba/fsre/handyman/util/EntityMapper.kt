package ba.fsre.handyman.util

internal abstract class EntityMapper<DTO, DomainModel> {

    abstract fun mapFromDto(dto: DTO): DomainModel

    open fun mapFromDtoList(dtoList: List<DTO>): List<DomainModel> {
        return dtoList.map { mapFromDto(it) }
    }

    // not needed
    /*fun mapToDto(domainModel: DomainModel): DTO*/
}

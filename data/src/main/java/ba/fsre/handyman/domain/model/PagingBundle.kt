package ba.fsre.handyman.domain.model

class PagingBundle<T>(
    val list: List<T>,
    val nextPage: Int,
    val totalDocs: Int
)
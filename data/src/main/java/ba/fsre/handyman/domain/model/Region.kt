package ba.fsre.handyman.domain.model

data class Region internal constructor(
    val id: String? = null,
    val name: String,
    val countryId: String
) {
    constructor(
        name: String,
        countryId: String
    ) : this(null, name, countryId)
}
package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import java.io.File

interface RequestRepository {
    suspend fun getAllOpenRequests(page: Int = 0, userId: String? = null): PagingBundle<Request>
    suspend fun create(request: Request, image: File? = null): Request
    suspend fun getByUser(userId: String, page: Int = 0): PagingBundle<Request>
    suspend fun markAsCompleted(requestId: String, userId: String): Request
    suspend fun accept(requestId: String, userId: String): Request
    suspend fun getUserJobs(userId: String, page: Int = 0): PagingBundle<Request>
    suspend fun delete(requestId: String)
}
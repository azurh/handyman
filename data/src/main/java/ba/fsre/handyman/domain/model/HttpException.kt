package ba.fsre.handyman.domain.model

import java.io.IOException

open class HandyHttpException(val statusCode: Int) : IOException("Belaj '${statusCode}'")
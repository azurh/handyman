package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.Category

interface CategoryRepository {
    suspend fun getAll(): List<Category>
    suspend fun getSingle(id: String): Category
    suspend fun addNew(category: Category): Category
    suspend fun delete(id: String)
}
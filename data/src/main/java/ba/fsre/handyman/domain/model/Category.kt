package ba.fsre.handyman.domain.model

data class Category internal constructor(
    val id: String? = null,
    var name: String,
    var description: String
) {
    constructor(
        name: String,
        description: String
    ) : this(null, name, description)
}
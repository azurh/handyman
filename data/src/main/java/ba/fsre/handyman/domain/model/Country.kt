package ba.fsre.handyman.domain.model

data class Country internal constructor(
    val id: String? = null,
    var name: String,
    var code: String,
) {
    constructor(
        name: String,
        code: String,
    ) : this(null, name, code)
}


package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.Service

interface ServiceRepository {
    suspend fun getAll(): List<Service>
    suspend fun getById(id: String): Service
    suspend fun create(service: Service): Service
    suspend fun delete(id: String)
}
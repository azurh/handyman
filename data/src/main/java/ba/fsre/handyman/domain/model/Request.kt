package ba.fsre.handyman.domain.model

data class Request internal constructor(
    val id: String? = null,
    var title: String,
    var description: String?,
    var completed: Boolean,
    var feedbackLeft: Boolean,
    var serviceId: String,
    var creatorId: String,
    var createdAt: String? = null,
    var servicerId: String? = null,
    var updatedAt: String? = null,
    var price: Double? = null,
    var startDate: String? = null,
    var originatorUsername: String? = null,
    var serviceName: String? = null,
    val image: String? = null
) {
    companion object {
        fun createNew(
            title: String,
            description: String? = null,
            startDate: String?,
            serviceId: String,
            creatorId: String,
            price: Double? = null
        ): Request {
            return Request(
                title = title,
                description = description,
                startDate = startDate,
                serviceId = serviceId,
                creatorId = creatorId,
                completed = false,
                feedbackLeft = false,
                price = price
            )
        }
    }
}

fun Request.hasImage() = image != null
package ba.fsre.handyman.domain.repository

interface AuthRepository {
    suspend fun login(email: String, password: String): String
}
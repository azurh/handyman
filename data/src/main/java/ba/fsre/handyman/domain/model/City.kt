package ba.fsre.handyman.domain.model

data class City(
    var name: String,
    val regionId: String
) {
    var id: String = ""
        internal set
}
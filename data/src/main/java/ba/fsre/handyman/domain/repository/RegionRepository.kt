package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.Region

interface RegionRepository {
    suspend fun getAll(): List<Region>
    suspend fun getById(id: String): Region
    suspend fun create(region: Region): Region
    suspend fun delete(id: String)
}
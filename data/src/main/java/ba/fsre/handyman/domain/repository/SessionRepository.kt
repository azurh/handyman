package ba.fsre.handyman.domain.repository

interface SessionRepository {
    fun saveToken(accessToken: String, refreshToken: String)
    fun obtainToken(): String?
    fun saveUserId(userId: String)
    fun obtainUserId(): String?
    fun clear()
}
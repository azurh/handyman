package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.City

interface CityRepository {
    suspend fun getAllCities(): List<City>
    suspend fun getCityById(cityId: String): City
    suspend fun addCity(city: City): City
    suspend fun updateCity(city: City): Boolean
    suspend fun delete(id: String)
}
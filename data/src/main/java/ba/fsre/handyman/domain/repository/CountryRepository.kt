package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.Country

interface CountryRepository {
    suspend fun getAll(): List<Country>
    suspend fun getById(id: String): Country
    suspend fun createNew(country: Country): Country
    suspend fun updateExisting(country: Country)
    suspend fun delete(id: String)
}
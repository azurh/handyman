package ba.fsre.handyman.domain.repository

import ba.fsre.handyman.domain.model.User

interface UserRepository {
    suspend fun register(user: User)
    suspend fun getAllUsers(): List<User>
    suspend fun getUserById(userId: String): User
    suspend fun addFcmToken(userId: String, fcmToken: String)
    suspend fun updateUserServices(userId: String, services: List<String>)
    suspend fun delete(userId: String)
}
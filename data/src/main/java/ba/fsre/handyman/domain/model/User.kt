package ba.fsre.handyman.domain.model

import ba.fsre.handyman.util.MD5Util

data class User internal constructor(
    var email: String,
    val id: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var password: String? = null,
    var address: String? = null,
    var phone: String? = null,
    var cityId: String,
    var cityName: String? = null,
    var services: List<String>? = null,
    var fcmTokens: List<String>? = null
) {
    constructor(
        email: String,
        firstName: String,
        lastName: String,
        password: String,
        cityId: String,
        cityName: String? = null,
        address: String? = null,
        phone: String? = null
    ) : this(
        email = email,
        id = null,
        firstName = firstName,
        lastName = lastName,
        password = password,
        address = address,
        phone = phone,
        cityId = cityId,
        cityName = cityName,
        services = listOf(),
        fcmTokens = listOf()
    )
}

fun User.gravatarUrl(): String {
    return "https://www.gravatar.com/avatar/${MD5Util.md5Hex(email)}?s=200"
}

fun User.displayName(): String {
    return "$firstName $lastName"
}

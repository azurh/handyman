package ba.fsre.handyman.domain.model

data class Service internal constructor(
    val id: String? = null,
    val name: String,
    val description: String? = null,
    val categoryId: String
) {
    constructor(
        name: String,
        description: String,
        categoryId: String
    ) : this(null, name, description, categoryId)

}

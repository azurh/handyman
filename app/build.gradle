import java.text.SimpleDateFormat

plugins {
    id 'com.android.application'
    id 'kotlin-android'
    id 'com.google.gms.google-services'
}
apply plugin: 'kotlin-android'

android {
    compileSdkVersion 30

    defaultConfig {
        applicationId "ba.fsre.handyman"
        minSdkVersion 23
        targetSdkVersion 30
        versionCode 8
        versionName "8"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    setProperty("archivesBaseName",
            new StringBuilder()
                    .append(rootProject.name)
                    .append("_${defaultConfig.versionName}")
                    .append("_v${defaultConfig.versionCode}")
                    .append("_${new SimpleDateFormat("ddMMYYY_HHmm").format(System.currentTimeMillis())}")
    )

    signingConfigs {

        release {
            storeFile file(RELEASE_STORE_FILE)
            storePassword RELEASE_STORE_PASSWORD
            keyAlias RELEASE_KEY_ALIAS
            keyPassword RELEASE_KEY_PASSWORD

            // Optional, specify signing versions used
            v1SigningEnabled true
            v2SigningEnabled true
        }
    }

    buildTypes {
        release {
            minifyEnabled false
            signingConfig signingConfigs.release
            // proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }

    buildFeatures {
        viewBinding true
    }

    flavorDimensions "distribute"
    productFlavors {
        appCenter {
            dimension "distribute"
        }
        googlePlay {
            dimension "distribute"
        }
    }

    packagingOptions {
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/license.txt'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/notice.txt'
        exclude 'META-INF/ASL2.0'
        exclude("META-INF/*.kotlin_module")
    }
}

dependencies {
    implementation project(path: ':data')

    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.3"

    implementation 'androidx.core:core-ktx:1.6.0'
    implementation 'androidx.appcompat:appcompat:1.3.0'
    implementation 'com.google.android.material:material:1.4.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.navigation:navigation-fragment-ktx:2.3.5'
    implementation 'androidx.navigation:navigation-ui-ktx:2.3.5'
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'

    implementation 'androidx.viewpager2:viewpager2:1.0.0'

    implementation 'me.relex:circleindicator:2.1.4'

    implementation "org.koin:koin-android:2.2.2"

    def arch_version = '2.2.0-alpha01'
    implementation "androidx.lifecycle:lifecycle-extensions:$arch_version"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$arch_version"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$arch_version"
    implementation "androidx.lifecycle:lifecycle-runtime-ktx:$arch_version"

    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'

    // Import the Firebase BoM
    implementation platform('com.google.firebase:firebase-bom:28.2.1')
    // Declare the dependency for the Firebase SDK for Google Analytics
    implementation 'com.google.firebase:firebase-analytics-ktx'

    implementation 'com.jakewharton.timber:timber:4.7.1'
    implementation 'com.google.code.gson:gson:2.8.7'

    implementation 'com.github.bumptech.glide:glide:4.12.0'
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0-alpha01"
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"

    implementation 'com.afollestad.material-dialogs:datetime:3.3.0'

    def appCenterSdkVersion = '4.3.1'
    implementation "com.microsoft.appcenter:appcenter-analytics:${appCenterSdkVersion}"
    implementation "com.microsoft.appcenter:appcenter-crashes:${appCenterSdkVersion}"
    // implementation "com.microsoft.appcenter:appcenter-distribute:${appCenterSdkVersion}"
    appCenterImplementation "com.microsoft.appcenter:appcenter-distribute:${appCenterSdkVersion}"
    googlePlayImplementation "com.microsoft.appcenter:appcenter-distribute-play:${appCenterSdkVersion}"

}
repositories {
    mavenCentral()
}
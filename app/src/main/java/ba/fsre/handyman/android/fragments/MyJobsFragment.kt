package ba.fsre.handyman.android.fragments

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.adapters.RequestsAdapter
import ba.fsre.handyman.android.databinding.FragmentAllRequestsBinding
import ba.fsre.handyman.android.utils.EndlessRecyclerViewScrollListener
import ba.fsre.handyman.android.utils.clear
import ba.fsre.handyman.android.utils.setOnClickListenerVisible
import ba.fsre.handyman.android.utils.show
import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber

class MyJobsFragment : BaseFragment<FragmentAllRequestsBinding>() {

    private val requestRepo: RequestRepository by inject()
    private val sessionRepo: SessionRepository by inject()
    private var latestPagingBundle: PagingBundle<Request>? = null

    private val adapter = RequestsAdapter(
        RequestsAdapter.Type.MY_JOBS,
        object : RequestsAdapter.RequestInteractionListener {
            override fun onAccept(request: Request) {

            }

            override fun onComplete(request: Request) {

            }

            override fun onMyJob(request: Request) {
                // Klik na ovo ne radi nista?
            }
        })

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentAllRequestsBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            binding.recyclerView.adapter = adapter
            binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
            binding.recyclerView.addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            binding.recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(
                binding.recyclerView.layoutManager as LinearLayoutManager
            ) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    if (latestPagingBundle?.totalDocs ?: Integer.MAX_VALUE < adapter.currentList.size)
                        fetchLatestJobs(latestPagingBundle!!.nextPage)
                }
            })
            binding.swipeRefreshLayout.setOnRefreshListener {
                adapter.clear()
                fetchLatestJobs(0)
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {
        fetchLatestJobs()
    }

    override fun provideScreenTitle() = getString(R.string.my_jobs)

    override fun provideTag() = TAG

    private fun fetchLatestJobs(page: Int = 0) {
        scope.launch {
            binding.swipeRefreshLayout.isRefreshing = true
            latestPagingBundle = requestRepo.getUserJobs(sessionRepo.obtainUserId()!!)
            adapter.submitList(
                adapter.currentList + latestPagingBundle!!.list.toMutableList()
            )
            toggleUiVisibility()
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun toggleUiVisibility() {
        binding.recyclerView.show(adapter.currentList.isNotEmpty())
        binding.viewEmptyList.root.show(adapter.currentList.isEmpty())
        binding.viewEmptyList.text.text = Html.fromHtml(getString(R.string.no_my_jobs))
        binding.viewEmptyList.text.setLinkTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.indigo
            )
        )
        binding.viewEmptyList.text.setOnClickListenerVisible {
            baseActivity.screenNavigator().toAllRequestsScreen()
        }
    }


    companion object {
        const val TAG = "MyJobsFragment"
    }
}

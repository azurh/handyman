package ba.fsre.handyman.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.FragmentEntryBinding

class EntryFragment : BaseFragment<FragmentEntryBinding>() {

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentEntryBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            binding.login.setOnClickListener {
                baseActivity.screenNavigator().toLoginScreen()
            }
            binding.register.setOnClickListener {
                baseActivity.screenNavigator().toRegisterScreen()
            }
            binding.guestAccess.setOnClickListener {
                baseActivity.screenNavigator().toDashboardScreen()
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {

    }

    override fun provideScreenTitle() = getString(R.string.welcome)

    override fun provideTag() = TAG

    companion object {
        const val TAG = "EntryFragment"
    }


}

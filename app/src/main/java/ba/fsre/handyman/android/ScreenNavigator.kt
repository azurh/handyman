package ba.fsre.handyman.android

interface ScreenNavigator {
    fun toLoginScreen()
    fun toRegisterScreen()
    fun toDashboardScreen()
    fun toNewRequestScreen()
    fun toMyJobsScreen()
    fun toMyRequestsScreen()
    fun toAllRequestsScreen()
}

package ba.fsre.handyman.android.adapters

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.ListItemRequestBinding
import ba.fsre.handyman.android.utils.hide
import ba.fsre.handyman.android.utils.show
import ba.fsre.handyman.domain.model.Request
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class RequestsAdapter(
    private val type: Type,
    private val requestInteractionListener: RequestInteractionListener? = null
) :
    ListAdapter<Request, RequestsAdapter.ViewHolder>(object : DiffUtil.ItemCallback<Request?>() {
        override fun areItemsTheSame(oldItem: Request, newItem: Request): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Request, newItem: Request): Boolean {
            return (oldItem.title == newItem.title) && (oldItem.description == newItem.description)
        }
    }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemRequestBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ViewHolder(private val binding: ListItemRequestBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(request: Request) {
            binding.root.setOnClickListener {
                when (type) {
                    Type.ALL_REQUESTS -> requestInteractionListener?.onAccept(request)
                    Type.MY_REQUESTS -> requestInteractionListener?.onComplete(request)
                    Type.MY_JOBS -> requestInteractionListener?.onMyJob(request)
                }
            }
            binding.name.text = request.title
            with(binding.description) {
                request.description.let { description ->
                    if (description.isNullOrBlank().not()) {
                        text = description
                        show()
                    } else {
                        hide()
                    }
                }
            }
            binding.serviceName.text = request.serviceName
            request.price.let { price ->
                if (price != null) {
                    binding.noPrice.hide()
                    binding.price.text = if (price.toFloat() == price.toInt().toFloat()) {
                        price.toInt().toString()
                    } else {
                        val df = DecimalFormat(
                            "#.00",
                            DecimalFormatSymbols.getInstance(Locale("hr"))
                        )
                        df.format(price)
                    }.let {
                        "$it KM"
                    }
                    binding.price.show()
                } else {
                    binding.price.hide()
                    binding.noPrice.setText(R.string.price_per_agreement)
                    binding.noPrice.show()
                }
            }
            binding.completed.show(request.completed)
            if (request.completed) {
                binding.root.setOnClickListener(null)
                binding.root.background = null
                binding.decor.show()
            } else {
                val outValue = TypedValue()
                binding.root.context.theme
                    .resolveAttribute(
                        android.R.attr.selectableItemBackground,
                        outValue,
                        true
                    )
                binding.root.setBackgroundResource(outValue.resourceId)
                binding.decor.hide()
            }
            binding.camera.show(request.image != null)
        }
    }

    /*private fun showPopupMenu(anchorView: View) {
        val popup = PopupMenu(anchorView.context, anchorView)
        popup.menuInflater.inflate()
    }*/

    interface RequestInteractionListener {
        fun onAccept(request: Request)
        fun onComplete(request: Request)
        fun onMyJob(request: Request)
    }

    enum class Type {
        ALL_REQUESTS,
        MY_REQUESTS,
        MY_JOBS
    }

}
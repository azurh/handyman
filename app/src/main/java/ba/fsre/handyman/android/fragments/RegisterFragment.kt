package ba.fsre.handyman.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.FragmentRegisterBinding
import ba.fsre.handyman.android.utils.hideKeyboard
import ba.fsre.handyman.android.utils.isValid
import ba.fsre.handyman.android.utils.snack
import ba.fsre.handyman.android.utils.text
import ba.fsre.handyman.domain.model.User
import ba.fsre.handyman.domain.repository.AuthRepository
import ba.fsre.handyman.domain.repository.CityRepository
import ba.fsre.handyman.domain.repository.UserRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class RegisterFragment : BaseFragment<FragmentRegisterBinding>() {

    private val userRepository: UserRepository by inject()
    private val authRepository: AuthRepository by inject()
    private val cityRepository: CityRepository by inject()

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentRegisterBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            scope.launch {
                val cities = cityRepository.getAllCities()
                val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                    requireContext(),
                    android.R.layout.select_dialog_item,
                    cities.map { it.name }
                )
                with(binding.cityInputLayout.editText as AutoCompleteTextView) {
                    setAdapter(adapter)
                    threshold = 0
                    setOnItemClickListener { adapterView, view, i, l ->
                        cities.find { it.name == (view as TextView).text }?.let {
                            tag = it.id
                        }
                    }
                }
            }
        }

        binding.registerButton.setOnClickListener {
            registerNewUser()
        }
    }

    private fun registerNewUser() {
        hideKeyboard()
        if (!validateForm()) {
            snack(getString(R.string.please_enter_all_required_fields))
            return
        }
        scope.launch {
            binding.progress.show()
            try {
                userRepository.register(
                    User(
                        email = binding.emailInputLayout.text,
                        firstName = binding.firstnameInputLayout.text,
                        lastName = binding.lastnameInputLayout.text,
                        password = binding.passwordInputLayout.text,
                        cityId = binding.cityInputLayout.editText!!.tag.toString(),
                        address = binding.addressInputLayout.text,
                        phone = binding.phoneInputLayout.text
                    )
                )
                delay(1000)
                authRepository.login(
                    email = binding.emailInputLayout.text,
                    password = binding.passwordInputLayout.text
                )
                delay(1000)
                baseActivity.close()
                baseActivity.screenNavigator().toDashboardScreen()
            } catch (e: Exception) {
                e.printStackTrace()
                snack(e.message ?: getString(R.string.generic_error))
            } finally {
                binding.progress.hide()
            }
        }
    }

    private fun validateForm(): Boolean {
        with(binding) {
            return emailInputLayout.isValid()
                    && passwordInputLayout.isValid()
                    && firstnameInputLayout.isValid()
                    && lastnameInputLayout.isValid()
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {

    }

    override fun provideScreenTitle() = getString(R.string.registration)

    override fun provideTag() = TAG

    companion object {
        const val TAG = "RegisterFragment"
    }
}

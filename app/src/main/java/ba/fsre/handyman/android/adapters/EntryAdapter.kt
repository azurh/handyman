package ba.fsre.handyman.android.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import ba.fsre.handyman.android.R

class EntryAdapter(
    private val items: List<Item>
) :
    RecyclerView.Adapter<EntryAdapter.ViewHolder>() {

    var onProceedClickListener: () -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_page, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemTitle.text = items[position].title
        holder.itemSubtitle.text = items[position].subtitle
        holder.itemImage.setImageResource(items[position].image)
        if (items[position].color != -1) {
            holder.itemImage.setColorFilter(
                items[position].color, android.graphics.PorterDuff.Mode.SRC_IN
            )
        }
        if (position == items.size - 1) {
            holder.itemProceed.visibility = View.VISIBLE
            holder.itemProceed.setOnClickListener { onProceedClickListener() }
        } else {
            holder.itemProceed.visibility = View.INVISIBLE
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemTitle: TextView = itemView.findViewById(R.id.title)
        val itemSubtitle: TextView = itemView.findViewById(R.id.sub_title)
        val itemImage: ImageView = itemView.findViewById(R.id.image)
        val itemProceed: TextView = itemView.findViewById(R.id.proceed)
    }

    class Item(
        val title: String,
        val subtitle: String,
        @DrawableRes val image: Int,
        val color: Int = -1,
    )
}
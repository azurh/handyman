package ba.fsre.handyman.android.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ba.fsre.handyman.android.utils.Prefs
import ba.fsre.handyman.domain.repository.SessionRepository
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {

    private val sessionRepo: SessionRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!Prefs.getInstance().hasSeenTourScreen) {
            TourActivity.start(this)
        } else if (sessionRepo.obtainToken() == null) {
            EntryActivity.start(this)
        } else {
            MainActivity.start(this)
        }
        finish()

    }
}
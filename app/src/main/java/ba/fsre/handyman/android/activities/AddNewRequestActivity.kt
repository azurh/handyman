package ba.fsre.handyman.android.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.ActivityAddNewRequestBinding
import ba.fsre.handyman.android.fragments.BaseFragment
import ba.fsre.handyman.android.fragments.NewRequestFragment
import ba.fsre.handyman.android.utils.hide
import ba.fsre.handyman.android.utils.show
import timber.log.Timber

class AddNewRequestActivity : BaseActivityImpl<ActivityAddNewRequestBinding>() {

    override fun bindView() = ActivityAddNewRequestBinding.inflate(layoutInflater)

    override fun setViewBehaviour() {
        setSupportActionBar(binding.toolbar)
        supportFragmentManager.registerFragmentLifecycleCallbacks(object :
            FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                super.onFragmentResumed(fm, f)
                if (f is BaseFragment<*>) {
                    binding.toolbar.title = f.provideScreenTitle()
                    binding.toolbar.show()
                } else {
                    binding.toolbar.hide()
                }
            }
        }, true)

        binding.toolbar.navigationIcon = AppCompatResources.getDrawable(this, R.drawable.ic_back)
        binding.toolbar.navigationIcon?.setTint(getColor(R.color.white))
        binding.toolbar.setNavigationOnClickListener {
            super.onBackPressed()
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            addFragment(NewRequestFragment())
        }
    }

    companion object {
        private const val TAG = "AddNewRequestActivity"

        fun start(context: Context) {
            context.startActivity(Intent(context, AddNewRequestActivity::class.java))
        }
    }
}
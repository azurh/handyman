package ba.fsre.handyman.android.utils

import android.content.Context
import androidx.core.content.edit

class Prefs private constructor(context: Context) {

    private var sharedPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    var hasSeenTourScreen: Boolean
        get() = sharedPrefs.getBoolean(KEY_SEEN_TOUR_SCREEN, false)
        set(value) {
            sharedPrefs.edit(commit = true) {
                putBoolean(KEY_SEEN_TOUR_SCREEN, value)
            }
        }

    companion object {
        private const val PREFS_NAME = "handyman_prefs"
        private const val KEY_SEEN_TOUR_SCREEN = "seen_tour_screen"

        private lateinit var instance: Prefs

        fun init(context: Context) {
            instance = Prefs(context)
        }

        fun getInstance(): Prefs {
            return instance
        }
    }
}

package ba.fsre.handyman.android

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import ba.fsre.handyman.android.utils.Prefs
import ba.fsre.handyman.di.DataDeps
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.distribute.Distribute
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        Prefs.init(this)
        AppCenter.setLogLevel(Log.VERBOSE)
        AppCenter.start(
            this,
            "fb5a0f36-7826-4b7e-b33b-45fa218b4825",
            Analytics::class.java,
            Distribute::class.java
        )

        startKoin {
            androidContext(this@App)
            modules(DataDeps.modules + DataDeps.cmodule)
        }

    }

    companion object {
        fun triggerRebirth(activity: Activity) {
            val packageManager: PackageManager = activity.packageManager
            val intent = packageManager.getLaunchIntentForPackage(activity.packageName)
            val componentName = intent!!.component
            val mainIntent = Intent.makeRestartActivityTask(componentName)
            activity.startActivity(mainIntent)
            Runtime.getRuntime().exit(0)
        }
    }
}
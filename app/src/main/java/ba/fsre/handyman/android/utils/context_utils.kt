package ba.fsre.handyman.android.utils

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

fun Context.isNightModeNow(): Boolean {
    val nightModeFlags: Int = resources.configuration.uiMode and
            Configuration.UI_MODE_NIGHT_MASK
    return when (nightModeFlags) {
        Configuration.UI_MODE_NIGHT_YES -> true
        Configuration.UI_MODE_NIGHT_NO -> false
        else -> false
    }
}

fun Fragment.isNightModeNow(): Boolean {
    return requireActivity().isNightModeNow()
}

fun Activity.toggleDayNightMode() {
    if (isNightModeNow()) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    } else {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Fragment.toast(message: String) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
}

fun Activity.snack(message: String) {
    Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show()
}

fun Fragment.snack(message: String) {
    requireActivity().snack(message)
}

fun Activity.hideKeyboard() {
    currentFocus?.clearFocus()
    rootView.isFocusable = true
    rootView.isFocusableInTouchMode = true
    rootView.requestFocus()
    hideKeyboard(currentFocus ?: rootView)
    rootView.isFocusable = false
    rootView.isFocusableInTouchMode = false
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

inline fun <reified T> Activity.args(keyName: String? = null): T {
    return intent.extras?.get(keyName!!) as T
}

inline fun <reified T> Fragment.args(keyName: String? = null): T {
    return arguments?.get(keyName!!) as T
}

package ba.fsre.handyman.android.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.BuildConfig
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.activities.PhotoViewActivity
import ba.fsre.handyman.android.databinding.FragmentNewRequestBinding
import ba.fsre.handyman.android.utils.*
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.ServiceRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import ba.fsre.handyman.util.date_utils.toIso8601
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.dateTimePicker
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class NewRequestFragment : BaseFragment<FragmentNewRequestBinding>() {

    private val REQUEST_IMAGE_CAPTURE = 301

    private val requestRepo: RequestRepository by inject()
    private val sessionRepo: SessionRepository by inject()
    private val servicesRepo: ServiceRepository by inject()

    lateinit var currentPhotoPath: String
    lateinit var camUri: Uri
    var resizedFile: File? = null

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentNewRequestBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            fetchAvailableServicesDropdown()
            setDateTimeClickListener()
            setPriceClickListeners()
            binding.confirmButton.setOnClickListener { createNewRequest() }
        }
    }

    private fun setPriceClickListeners() {
        binding.pricePerAgreement.setOnCheckedChangeListener { buttonView, isChecked ->
            binding.priceInputLayout.isEnabled = !isChecked
            binding.priceInputLayout.editText!!.isEnabled = !isChecked
        }
        binding.attachmentButton.setOnClickListener {
            takePicture()
        }
    }

    private fun takePicture() {
        checkCameraPermission {
            dispatchTakePictureIntent()
        }
    }

    private fun checkCameraPermission(onGranted: () -> Unit) {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 661)
        } else {
            onGranted()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 661) {
            if (permissions[0] == Manifest.permission.CAMERA && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            }
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            // Create the File where the photo should go
            val photoFile: File? = try {
                createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
                null
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, camUri)
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            val originalBitmap = BitmapFactory.decodeFile(currentPhotoPath)

            File.createTempFile(
                "photo_${SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())}",
                ".jpg",
                requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
            ).also {

                resizedFile = it

                /*resizedFile = File(
                    requireContext().externalCacheDir!!,
                    "compressed_${
                        UUID.randomUUID().toString().take(8).lowercase()
                    }.jpg",
                )*/

                val exifInterface = ExifInterface(currentPhotoPath)
                val orientation =
                    exifInterface.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                    )
                val angle = when (orientation) {
                    ExifInterface.ORIENTATION_NORMAL -> 0
                    ExifInterface.ORIENTATION_ROTATE_90 -> 90
                    ExifInterface.ORIENTATION_ROTATE_180 -> 180
                    ExifInterface.ORIENTATION_ROTATE_270 -> 270
                    else -> 0
                }

                val matrix = Matrix().apply { postRotate(angle.toFloat()) }

                val resizedBitmap = originalBitmap.resize(1000, 1000)

                val okBitmap = Bitmap.createBitmap(
                    resizedBitmap,
                    0,
                    0,
                    resizedBitmap.width,
                    resizedBitmap.height,
                    matrix,
                    true
                )

                okBitmap.compress(
                    Bitmap.CompressFormat.JPEG, 100, BufferedOutputStream(
                        FileOutputStream(resizedFile)
                    )
                )

                binding.filename.text = resizedFile!!.name
                binding.filename.setOnClickListenerVisible {
                    PhotoViewActivity.start(requireContext(), resizedFile!!.absolutePath)
                }
                binding.imageDismiss.setOnClickListener {
                    resizedFile = null
                    binding.imageFileGroup.toggleTransition(false)
                }
                binding.imageFileGroup.show()
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        // val storageDir: File = requireContext().filesDir
        val storageDir: File =
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            camUri = FileProvider.getUriForFile(
                requireContext(),
                BuildConfig.APPLICATION_ID + ".fileProvider",
                this
            )
            currentPhotoPath = absolutePath
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setDateTimeClickListener() {
        with(binding.datetimeInputLayout.editText!!) {
            setOnClickListener {
                showDateTimeDialog {
                    setText(it.toReadableFormat())
                    tag = Date(it.timeInMillis).toIso8601()
                }
            }
        }
    }

    private fun showDateTimeDialog(onDateTimeChosen: (Calendar) -> Unit) {
        MaterialDialog(requireContext()).show {
            title(R.string.choose_date_time)
            dateTimePicker(
                requireFutureDateTime = true,
                show24HoursView = true,
                autoFlipToTime = false
            ) { _, dateTime ->
                onDateTimeChosen(dateTime)
            }
        }
    }

    private fun createNewRequest() {
        hideKeyboard()
        if (!validateForm()) {
            snack(getString(R.string.please_enter_all_required_fields))
            return
        }
        scope.launch {
            binding.progress.show()
            try {
                requestRepo.create(
                    Request.createNew(
                        title = binding.titleInputLayout.text,
                        description = binding.descriptionInputLayout.text,
                        startDate = binding.datetimeInputLayout.editText!!.tag!!.toString(),
                        serviceId = binding.serviceInputLayout.editText!!.tag.toString(),
                        creatorId = sessionRepo.obtainUserId()!!,
                        price = obtainPrice(),
                    ),
                    image = resizedFile
                )
                activity?.onBackPressed()
            } catch (e: Exception) {
                snack(e.message ?: getString(R.string.generic_error))
            } finally {
                binding.progress.hide()
            }

        }
    }

    private fun validateForm(): Boolean {
        with(binding) {
            return titleInputLayout.isValid()
                    && serviceInputLayout.isValid()
                    && datetimeInputLayout.isValid()
                    && isPriceValid()
        }
    }

    private fun isPriceValid(): Boolean {
        with(binding) {
            return priceInputLayout.isValid() || pricePerAgreement.isChecked
        }
    }

    private fun obtainPrice(): Double? {
        return if (!binding.pricePerAgreement.isChecked) {
            binding.priceInputLayout.text.toDouble()
        } else {
            null
        }
    }

    private fun fetchAvailableServicesDropdown() {
        scope.launch {
            val services = servicesRepo.getAll()
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                requireContext(),
                android.R.layout.select_dialog_item,
                services.map { it.name }
            )
            with(binding.serviceInputLayout.editText as AutoCompleteTextView) {
                setAdapter(adapter)
                threshold = 0
                setOnItemClickListener { adapterView, view, i, l ->
                    services.find { it.name == (view as TextView).text }?.let {
                        tag = it.id
                    }
                }
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {

    }

    override fun provideScreenTitle() = getString(R.string.add_new_request)

    override fun provideTag() = TAG

    companion object {
        const val TAG = "NewRequestFragment"
    }
}

private fun File.rotation(): String {
    val ei = ExifInterface(absolutePath)
    val orientation = ei.getAttributeInt(
        ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_UNDEFINED
    )
    return when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90 -> {
            "ORIENTATION_ROTATE_90"
        }
        ExifInterface.ORIENTATION_ROTATE_180 -> {
            "ORIENTATION_ROTATE_180"
        }
        ExifInterface.ORIENTATION_ROTATE_270 -> {
            "ORIENTATION_ROTATE_270"
        }
        ExifInterface.ORIENTATION_NORMAL -> {
            "ORIENTATION_NORMAL"
        }
        else -> {
            "/else/"
        }
    }
}

private fun Calendar.toReadableFormat(): String {
    return SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.US).format(timeInMillis)
}

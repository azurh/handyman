package ba.fsre.handyman.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.FragmentLoginBinding
import ba.fsre.handyman.android.utils.hideKeyboard
import ba.fsre.handyman.android.utils.snack
import ba.fsre.handyman.android.utils.text
import ba.fsre.handyman.domain.repository.AuthRepository
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val authRepository: AuthRepository by inject()

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentLoginBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            binding.loginButton.setOnClickListener {
                login {
                    baseActivity.close()
                    baseActivity.screenNavigator().toDashboardScreen()
                }
            }
        }
    }

    private fun login(callback: () -> Unit) {
        scope.launch {
            hideKeyboard()
            binding.progress.show()
            try {
                authRepository.login(
                    email = binding.emailInputLayout.text,
                    password = binding.passwordInputLayout.text,
                )
                callback.invoke()
            } catch (e: Exception) {
                snack(e.message ?: getString(R.string.generic_error))
            } finally {
                binding.progress.hide()
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {

    }

    override fun provideScreenTitle() = getString(R.string.login)

    override fun provideTag() = TAG

    companion object {
        const val TAG = "LoginFragment"
    }
}

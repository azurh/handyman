package ba.fsre.handyman.android.fragments

import android.animation.LayoutTransition
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.activities.BaseActivity
import ba.fsre.handyman.android.utils.listKeys
import ba.fsre.handyman.android.utils.snack
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

abstract class BaseFragment<VB : ViewBinding> : Fragment() {
    private var viewCached: Boolean = false

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    private var rootView: View? = null

    private val keepView = true

    private var _baseActivity: BaseActivity? = null
    protected val baseActivity get() = _baseActivity!!

    private var fragmentCreateCount = 0
    private var viewCreateCount = 0

    val isFromBackstack: Boolean
        get() = viewCreateCount > fragmentCreateCount

    private val coroutineExceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable, "CustomScopeError")
            snack(throwable.message ?: getString(R.string.generic_error))
        }

    // same as viewModelScope but with a centralized exception handler
    // useful for http errors as for now
    val scope = CoroutineScope(lifecycleScope.coroutineContext + coroutineExceptionHandler)

    protected abstract fun bindView(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): ViewBinding

    protected abstract fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?)
    protected abstract fun onReady(savedInstanceState: Bundle?)
    abstract fun provideTag(): String
    abstract fun provideScreenTitle(): String

    protected open fun extractArguments(arguments: Bundle) {}

    override fun onAttach(context: Context) {
        logLifecycle("onAttach")
        _baseActivity = context as BaseActivity
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        fragmentCreateCount++
        logLifecycle("onCreate ${savedInstanceState?.listKeys()}")
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            extractArguments(requireArguments())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewCreateCount++
        logLifecycle("onCreateView -> is view null:${view == null}")
        savedInstanceState?.let {
            logLifecycle("onCreateView -> state:${it.listKeys()}")
        }

        if (!keepView) {
            rootView = view
            _binding = bindView(layoutInflater, container) as VB
            rootView = binding.root
            rootView!!.isClickable = true
            viewCached = false
        } else {
            if (rootView != null) {
                viewCached = true
                return rootView
            } else {
                rootView = view
                if (rootView == null) {
                    _binding = bindView(layoutInflater, container) as VB
                    rootView = binding.root
                    rootView!!.isClickable = true
                    viewCached = false
                } else {
                    viewCached = true
                }
            }
        }

        return rootView.also {
            logLifecycle("ViewCheck: $it")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        logLifecycle("onViewCreated: ${savedInstanceState?.listKeys()}")
        super.onViewCreated(view, savedInstanceState)
        logLifecycle("setViewBehaviour, cached->${this.viewCached}")
        if (!keepView) {
            viewCached = false
        }
        setViewBehaviour(viewCached, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        logLifecycle("onActivityCreated")
        super.onActivityCreated(savedInstanceState)
        logState(savedInstanceState)
        onReady(savedInstanceState)
    }

    override fun onStart() {
        logLifecycle("onStart")
        super.onStart()
    }

    override fun onResume() {
        logLifecycle("onResume")
        super.onResume()
    }

    override fun onStop() {
        logLifecycle("onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        logLifecycle("onDestroyView")
        if (!keepView)
            _binding = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        logLifecycle("onDestroy")
        super.onDestroy()
    }

    override fun onDetach() {
        if (!keepView) {
            rootView = null
        }
        _baseActivity = null
        logLifecycle("onDetach")
        super.onDetach()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        logLifecycle("onConfigurationChanged")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        logLifecycle("onSaveInstanceState")
        super.onSaveInstanceState(outState)
    }

    private fun logLifecycle(message: String) {
        Timber.i("${javaClass.simpleName}: $message")
    }

    private fun logState(savedInstanceState: Bundle?) {
        Timber.i("Saved state: ${savedInstanceState?.listKeys()}")
    }

    protected fun goBack() {
        requireActivity().onBackPressed()
    }

    private fun animateLayoutChanges() {
        val lt = LayoutTransition()
        lt.enableTransitionType(LayoutTransition.CHANGE_APPEARING)
        lt.enableTransitionType(LayoutTransition.CHANGE_DISAPPEARING)
        lt.enableTransitionType(LayoutTransition.APPEARING)
        lt.enableTransitionType(LayoutTransition.DISAPPEARING)
        lt.enableTransitionType(LayoutTransition.CHANGING)
        (binding.root as ViewGroup).layoutTransition = lt
    }

    protected fun delayForTransitionTime(runnable: () -> (Unit)) {
        lifecycleScope.launch {
            delay(resources.getInteger(R.integer.screen_transition).toLong())
            runnable()
        }
    }

    /*private fun enableTransition() {
        val transition: Transition = Fade()
        transition.duration = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
        transition.addTarget(binding.progressBar)
        transition.addTarget(binding.password)
        TransitionManager.beginDelayedTransition(binding.root, transition)
    }*/
}

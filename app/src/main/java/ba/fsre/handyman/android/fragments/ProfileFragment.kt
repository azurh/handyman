package ba.fsre.handyman.android.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.App
import ba.fsre.handyman.android.BuildConfig
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.FragmentProfileBinding
import ba.fsre.handyman.android.utils.snack
import ba.fsre.handyman.android.utils.toggleTransition
import ba.fsre.handyman.domain.model.User
import ba.fsre.handyman.domain.model.gravatarUrl
import ba.fsre.handyman.domain.repository.AuthRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import ba.fsre.handyman.domain.repository.UserRepository
import com.bumptech.glide.Glide
import com.microsoft.appcenter.distribute.Distribute
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private val authRepo: AuthRepository by inject()
    private val userRepo: UserRepository by inject()
    private val sessionRepo: SessionRepository by inject()

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentProfileBinding.inflate(inflater, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            fetchLoggedInUserData {
                Glide.with(requireContext())
                    .load(it.gravatarUrl())
                    .circleCrop()
                    .into(binding.avatar)
                binding.fullName.text = "${it.firstName} ${it.lastName}"
                binding.email.text = it.email
                binding.address.text = "${it.address}, ${it.cityName}"
            }
            binding.logoutButton.setOnClickListener {
                sessionRepo.clear()
                App.triggerRebirth(requireActivity())
            }
            binding.version.text = "${BuildConfig.VERSION_NAME}_${BuildConfig.VERSION_CODE}"
            binding.version.setOnClickListener {
                Distribute.checkForUpdate()
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {

    }

    override fun provideScreenTitle() = getString(R.string.my_profile)

    override fun provideTag() = TAG

    private fun fetchLoggedInUserData(callback: (User) -> Unit) {
        scope.launch {
            binding.progress.toggleTransition(true)
            try {
                userRepo.getUserById(sessionRepo.obtainUserId()!!).let {
                    callback.invoke(it)
                }
            } catch (e: Exception) {
                snack(e.message ?: getString(R.string.generic_error))
            } finally {
                binding.progress.toggleTransition(false)
            }
        }
    }

    companion object {
        const val TAG = "ProfileFragment"
    }


}

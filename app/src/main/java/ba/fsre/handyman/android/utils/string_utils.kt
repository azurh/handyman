package ba.fsre.handyman.android.utils

fun String.wordCount(): Int {
    return split(" ").size
}

fun String.times(n: Int = 2): String {
    var s = this
    repeat(n) {
        s = s.plus(" ").plus(s)
    }
    return s
}

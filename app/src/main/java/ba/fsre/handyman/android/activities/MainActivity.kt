package ba.fsre.handyman.android.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.ActivityMainBinding
import ba.fsre.handyman.android.fragments.*
import ba.fsre.handyman.android.utils.getAuthPopup
import ba.fsre.handyman.domain.repository.SessionRepository
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.util.*

class MainActivity : BaseActivityImpl<ActivityMainBinding>() {

    private val sessionRepo: SessionRepository by inject()

    override fun bindView() = ActivityMainBinding.inflate(layoutInflater)

    override fun setViewBehaviour() {
        setSupportActionBar(binding.toolbar)
    }

    override fun onReady(savedInstanceState: Bundle?) {
        supportFragmentManager.registerFragmentLifecycleCallbacks(object :
            FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                super.onFragmentResumed(fm, f)
                if (f is BaseFragment<*>) {
                    binding.toolbar.title = f.provideScreenTitle()
                }
            }
        }, true)

        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_all_services -> showFragment(AllRequestsFragment())
                R.id.nav_my_services -> {
                    if (sessionRepo.obtainToken() == null) {
                        getAuthPopup(this).show()
                        return@setOnNavigationItemSelectedListener false
                    }
                    showFragment(MyRequestsFragment())
                }
                R.id.nav_my_jobs -> {
                    if (sessionRepo.obtainToken() == null) {
                        getAuthPopup(this).show()
                        return@setOnNavigationItemSelectedListener false
                    }
                    showFragment(MyJobsFragment())
                }
                R.id.nav_profile -> {
                    if (sessionRepo.obtainToken() == null) {
                        getAuthPopup(this).show()
                        return@setOnNavigationItemSelectedListener false
                    }
                    showFragment(ProfileFragment())
                }
            }
            true
        }
        binding.bottomNavigationView.selectedItemId = R.id.nav_all_services
    }

    private fun showFragment(baseFragment: BaseFragment<*>? = null) {
        if (baseFragment == null) {
            return
        }
        if (supportFragmentManager.isTopFragment(baseFragment)) {
            return
        }
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.fast_fade_in,
                R.anim.fast_fade_out,
                R.anim.fast_fade_in,
                R.anim.fast_fade_out,
            )
            .replace(
                R.id.fragment_container,
                baseFragment,
                baseFragment.provideTag()
            )
            .commit()
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.menu_auth)?.isVisible = sessionRepo.obtainUserId() == null
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_auth) {
            EntryActivity.start(this)
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }

    fun navToAllRequests() {
        binding.bottomNavigationView.selectedItemId = R.id.nav_all_services
    }

    fun navToMyRequests() {
        binding.bottomNavigationView.selectedItemId = R.id.nav_my_services
    }
}

fun FragmentManager.isTopFragment(fragment: Fragment): Boolean {
    Timber.tag("isTopFragment").i(fragments.toString())
    if (fragments.isEmpty()) {
        return false
    }
    if (fragment !is BaseFragment<*>) {
        return false
    }
    return fragments.last().tag == fragment.provideTag()
}


fun FragmentManager.contains(fragment: Fragment): Boolean {
    if (fragments.isEmpty()) {
        return false
    }
    if (fragment !is BaseFragment<*>) {
        return false
    }
    return fragments.find { it.tag == fragment.provideTag() } != null
}


package ba.fsre.handyman.android.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.transition.Fade
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.google.android.material.textfield.TextInputLayout

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.show(show: Boolean) {
    if (show) show() else hide()
}

fun View.toggle() {
    show(visibility == View.GONE)
}

fun View.toggleTransition(show: Boolean) {
    val transition: Transition = Fade()
    transition.duration = 300
    transition.addTarget(this)
    TransitionManager.beginDelayedTransition(this.parent as ViewGroup, transition)
    // val show = visibility != View.VISIBLE
    visibility = if (show) View.VISIBLE else View.GONE
}

fun TextView.clear() {
    text = ""
}

val Activity.rootView: View
    get() = findViewById(android.R.id.content)

internal fun TextInputLayout.clear() {
    error = ""
    endIconDrawable = null
    startIconDrawable = null
}

@SuppressLint("ClickableViewAccessibility")
fun View.setOnClickListenerVisible(callback: (view: View) -> Unit) {
    setOnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                v.alpha = 0.5f
                true
            }
            MotionEvent.ACTION_UP -> {
                v.alpha = 1.0f
                callback.invoke(v)
                true
            }
            MotionEvent.ACTION_CANCEL -> {
                v.alpha = 1.0f
                true
            }
            else -> {
                false
            }
        }
    }
}

/*fun <T> Activity.args(keyName: String? = null): ReadOnlyProperty<Activity, T?> {
    return object : ReadOnlyProperty<Activity, T> {
        override operator fun getValue(thisRef: Activity, property: KProperty<*>): T {
            return thisRef.intent.extras?.get(keyName ?: property.name) as T
        }
    }
}*/

fun EditText.trimmedText(): String {
    return text.trim().toString()
}

fun SwitchCompat.check(
    check: Boolean,
    notify: Boolean,
    listener: CompoundButton.OnCheckedChangeListener
) {
    if (notify) {
        this.setOnCheckedChangeListener(listener)
        this.isChecked = check
        this.setOnCheckedChangeListener(null)
    } else {
        this.setOnCheckedChangeListener(null)
        this.isChecked = check
        this.setOnCheckedChangeListener(listener)
    }
}

fun SwitchCompat.toggle(
    notify: Boolean,
    listener: CompoundButton.OnCheckedChangeListener
) {
    if (notify) {
        this.setOnCheckedChangeListener(listener)
        this.isChecked = !isChecked
        this.setOnCheckedChangeListener(null)
    } else {
        this.setOnCheckedChangeListener(null)
        this.isChecked = !isChecked
        this.setOnCheckedChangeListener(listener)
    }
}

fun EditText.popKeyboard() {
    requestFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)
        ?.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

var TextInputLayout.text: String
    get() = editText!!.trimmedText()
    set(value) {
        editText!!.setText(value)
    }

fun ListAdapter<*, *>.clear() {
    submitList(listOf())
}

fun TextInputLayout.isValid(): Boolean {
    return !text.isBlank()
}
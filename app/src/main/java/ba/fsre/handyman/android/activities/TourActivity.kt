package ba.fsre.handyman.android.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.adapters.EntryAdapter
import ba.fsre.handyman.android.databinding.ActivityTourBinding
import ba.fsre.handyman.android.utils.Prefs

class TourActivity : AppCompatActivity() {

    lateinit var binding: ActivityTourBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTourBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setViewPager()
    }

    private fun generateItems(): List<EntryAdapter.Item> {
        return listOf(
            EntryAdapter.Item(
                title = "Pretraga",
                subtitle = "Pretraži kućanske poslove objavljene iz nekoliko različitih kategorija \uD83E\uDDF9 \uD83E\uDDFD ",
                image = R.drawable.ic_services,
                color = ContextCompat.getColor(this, R.color.indigo)
            ),
            EntryAdapter.Item(
                title = "Objavi zahtjev",
                subtitle = "Ne radi utičnica? Treba očistiti odžak?\nObjavite zahtjev, dodajte sliku i cijenu ⚙️ \uD83D\uDD27",
                image = R.drawable.ic_toolbox_outline,
                color = ContextCompat.getColor(this, R.color.roman)
            ),
            EntryAdapter.Item(
                title = "Obavi posao",
                subtitle = "Možeš okrečiti sobu? Znaš popraviti bojler?\nPretraži poslove po lokaciji i kategoriji \uD83E\uDDF0 \uD83D\uDCCD",
                image = R.drawable.ic_format_paint,
                color = ContextCompat.getColor(this, R.color.laser)
            ),
            EntryAdapter.Item(
                title = "Handyman",
                subtitle = "",
                image = R.mipmap.ic_launcher
            )
        )
    }

    private fun setViewPager() {
        binding.viewPager.adapter = EntryAdapter(generateItems()).apply {
            onProceedClickListener = { closeTourScreen() }
        }
        binding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL

        binding.indicator.setViewPager(binding.viewPager)
    }

    override fun onBackPressed() {
        closeTourScreen()
    }

    private fun closeTourScreen() {
        Prefs.getInstance().hasSeenTourScreen = true
        EntryActivity.start(this)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, TourActivity::class.java))
        }
    }
}

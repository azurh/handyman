package ba.fsre.handyman.android.activities

import ba.fsre.handyman.android.ScreenNavigator
import ba.fsre.handyman.android.fragments.LoginFragment
import ba.fsre.handyman.android.fragments.MyJobsFragment
import ba.fsre.handyman.android.fragments.RegisterFragment

class ScreenNavigatorImpl(private val baseActivity: BaseActivity) : ScreenNavigator {

    override fun toLoginScreen() {
        baseActivity.addFragment(LoginFragment())
    }

    override fun toRegisterScreen() {
        baseActivity.addFragment(RegisterFragment())
    }

    override fun toDashboardScreen() {
        MainActivity.start(baseActivity.context())
    }

    override fun toNewRequestScreen() {
        AddNewRequestActivity.start(baseActivity.context())
    }

    override fun toMyJobsScreen() {
        baseActivity.addFragment(MyJobsFragment())
    }

    override fun toMyRequestsScreen() {
        if (baseActivity is MainActivity) {
            baseActivity.navToMyRequests()
        }
    }

    override fun toAllRequestsScreen() {
        if (baseActivity is MainActivity) {
            baseActivity.navToAllRequests()
        }
    }

}

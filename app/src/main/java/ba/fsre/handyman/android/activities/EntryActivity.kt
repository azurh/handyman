package ba.fsre.handyman.android.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ba.fsre.handyman.android.databinding.ActivityEntryBinding
import ba.fsre.handyman.android.fragments.BaseFragment
import ba.fsre.handyman.android.fragments.EntryFragment
import ba.fsre.handyman.android.utils.hide
import ba.fsre.handyman.android.utils.show
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class EntryActivity : BaseActivityImpl<ActivityEntryBinding>() {

    override fun bindView() = ActivityEntryBinding.inflate(layoutInflater)

    override fun setViewBehaviour() {
        setSupportActionBar(binding.toolbar)
        supportFragmentManager.registerFragmentLifecycleCallbacks(object :
            FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                super.onFragmentResumed(fm, f)
                if (f is BaseFragment<*>) {
                    binding.toolbar.title = f.provideScreenTitle()
                    binding.toolbar.show()
                } else {
                    binding.toolbar.hide()
                }
            }
        }, true)
    }

    override fun onReady(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            addFragment(EntryFragment())
        }
    }

    companion object {
        private const val TAG = "EntryActivity"

        fun start(context: Context) {
            context.startActivity(Intent(context, EntryActivity::class.java))
        }
    }
}
package ba.fsre.handyman.android.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.databinding.ActivityPhotoViewBinding
import ba.fsre.handyman.android.utils.args
import com.bumptech.glide.Glide
import timber.log.Timber

class PhotoViewActivity : BaseActivityImpl<ActivityPhotoViewBinding>() {

    override fun bindView() = ActivityPhotoViewBinding.inflate(layoutInflater)

    override fun setViewBehaviour() {
        setSupportActionBar(binding.toolbar)

        binding.toolbar.navigationIcon = AppCompatResources.getDrawable(this, R.drawable.ic_back)
        binding.toolbar.navigationIcon?.setTint(getColor(R.color.white))
        binding.toolbar.setNavigationOnClickListener {
            super.onBackPressed()
        }
        binding.toolbar.setBackgroundColor(Color.BLACK)
    }

    override fun onReady(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {

            val urlString = args<String>(ARGS_IMAGE_URL)

            Timber.i("urlString $urlString")

            binding.toolbar.title = Uri.parse(urlString).encodedPath

            Glide.with(this)
                .load(urlString)
                .into(binding.image)
        }
    }

    companion object {
        private const val TAG = "AddNewRequestActivity"
        private const val ARGS_IMAGE_URL = "ARGS_IMAGE_URL"

        fun start(context: Context, imageUrl: String) {
            context.startActivity(Intent(context, PhotoViewActivity::class.java).apply {
                putExtra(ARGS_IMAGE_URL, imageUrl)
            })
        }
    }
}
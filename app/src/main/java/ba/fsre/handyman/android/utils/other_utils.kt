package ba.fsre.handyman.android.utils

import android.app.ActivityManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.Config.ARGB_8888
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.activities.EntryActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.GsonBuilder
import java.io.IOException
import java.net.URL

fun Bundle.listKeys() = keySet().toList().toString()

fun Any?.gsonize(prettyPrint: Boolean = false): String {
    return GsonBuilder().apply {
        if (prettyPrint)
            setPrettyPrinting()
    }.create()
        .toJson(this)
}

fun URL.toBitmap(): Bitmap? {
    return try {
        BitmapFactory.decodeStream(openStream())
    } catch (e: IOException) {
        null
    }
}

fun Bitmap.makeSquare(): Bitmap {
    val size = maxOf(this.width, this.height)

    val outputImage = Bitmap.createBitmap(size, size, ARGB_8888)
    val can = Canvas(outputImage)
    can.drawARGB(0, 0, 0, 0)

    can.drawBitmap(this, (size - this.width) / 2f, (size - this.height) / 2f, null)
    return outputImage
}

fun isServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
    val manager = context.getSystemService(AppCompatActivity.ACTIVITY_SERVICE) as ActivityManager
    for (service in manager.getRunningServices(Int.MAX_VALUE)) {
        if (serviceClass.name == service.service.className) {
            return true
        }
    }
    return false
}

fun getAuthPopup(context: Context): MaterialDialog {
    return MaterialDialog(context).apply {
        title(res = R.string.app_name)
        message(res = R.string.login_or_register)
        positiveButton(res = R.string.login) {
            EntryActivity.start(context)
        }
        negativeButton(res = R.string.go_back) {
            it.dismiss()
        }
    }
}

fun Bitmap.resize(maxWidth: Int, maxHeight: Int): Bitmap {
    return if (maxHeight > 0 && maxWidth > 0) {
        val width = width
        val height = height
        val ratioBitmap = width.toFloat() / height.toFloat()
        val ratioMax = maxWidth.toFloat() / maxHeight.toFloat()
        var finalWidth = maxWidth
        var finalHeight = maxHeight
        if (ratioMax > ratioBitmap) {
            finalWidth = (maxHeight.toFloat() * ratioBitmap).toInt()
        } else {
            finalHeight = (maxWidth.toFloat() / ratioBitmap).toInt()
        }
        Bitmap.createScaledBitmap(this, finalWidth, finalHeight, true)
    } else {
        this
    }
}


package ba.fsre.handyman.android.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.ScreenNavigator
import ba.fsre.handyman.android.fragments.BaseFragment
import ba.fsre.handyman.android.utils.hideKeyboard
import timber.log.Timber

abstract class BaseActivityImpl<VB : ViewBinding> : AppCompatActivity(), BaseActivity {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    private val screenNavigator: ScreenNavigator by lazy {
        ScreenNavigatorImpl(this)
    }

    protected abstract fun bindView(): ViewBinding

    protected abstract fun setViewBehaviour()
    protected abstract fun onReady(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("$savedInstanceState")
        _binding = bindView() as VB
        setContentView(binding.root)
        setViewBehaviour()
        onReady(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            hideKeyboard()
            super.onBackPressed()
        } else {
            finish()
        }
    }

    override fun addFragment(baseFragment: BaseFragment<*>) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out,
                android.R.anim.fade_in,
                android.R.anim.fade_out,
            )
            .replace(
                R.id.fragment_container,
                baseFragment,
                baseFragment.provideTag()
            )
            .addToBackStack(baseFragment.provideTag())
            .commit()
    }

    override fun screenNavigator(): ScreenNavigator {
        return screenNavigator
    }

    override fun close() {
        finishAffinity()
    }

    override fun context() = this
}

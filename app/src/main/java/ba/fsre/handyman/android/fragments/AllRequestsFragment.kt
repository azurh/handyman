package ba.fsre.handyman.android.fragments

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.activities.PhotoViewActivity
import ba.fsre.handyman.android.adapters.RequestsAdapter
import ba.fsre.handyman.android.databinding.DialogAcceptRequestBinding
import ba.fsre.handyman.android.databinding.FragmentAllRequestsBinding
import ba.fsre.handyman.android.utils.*
import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.model.displayName
import ba.fsre.handyman.domain.model.hasImage
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.ServiceRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import ba.fsre.handyman.domain.repository.UserRepository
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class AllRequestsFragment : BaseFragment<FragmentAllRequestsBinding>() {

    private val requestRepo: RequestRepository by inject()
    private val sessionRepo: SessionRepository by inject()
    private val userRepo: UserRepository by inject()
    private val serviceRepo: ServiceRepository by inject()

    private var latestPagingBundle: PagingBundle<Request>? = null

    private val adapter = RequestsAdapter(
        RequestsAdapter.Type.ALL_REQUESTS,
        object : RequestsAdapter.RequestInteractionListener {
            override fun onAccept(request: Request) {
                showAcceptPrompt(request) {
                    scope.launch {
                        requestRepo.accept(
                            requestId = request.id!!,
                            userId = sessionRepo.obtainUserId()!!
                        )
                        notifyRemoved(request)
                    }
                }
            }

            override fun onComplete(request: Request) {}
            override fun onMyJob(request: Request) {}
        })

    private fun notifyRemoved(request: Request) {
        val list = adapter.currentList.toMutableList()
        list.removeAt(list.indexOf(list.find { it.id == request.id!! }))
        adapter.submitList(list) {
            toggleUiVisibility()
        }
    }

    private fun showAcceptPrompt(request: Request, onDone: (Request) -> Unit) {
        if (sessionRepo.obtainToken() == null) {
            getAuthPopup(requireContext()).show()
            return
        }
        scope.launch {
            val bottomSheetDialog = BottomSheetDialog(requireContext())
            val customViewBinding = DialogAcceptRequestBinding.inflate(layoutInflater)
            bottomSheetDialog.setContentView(customViewBinding.root)

            customViewBinding.progress.toggleTransition(true)

            bottomSheetDialog.show()

            customViewBinding.title.text = request.title
            customViewBinding.description.text = request.description

            val user = userRepo.getUserById(request.creatorId)

            if (request.hasImage()) {
                customViewBinding.image.show()
                Glide.with(requireContext())
                    .load(request.image)
                    .into(customViewBinding.image)
                customViewBinding.image.setOnClickListenerVisible {
                    PhotoViewActivity.start(requireContext(), request.image!!)
                }
            }

            customViewBinding.user.text = user.displayName()
            customViewBinding.phone.text = user.phone
            customViewBinding.email.text = user.email

            customViewBinding.progress.toggleTransition(false)
            customViewBinding.groupContent.toggleTransition(true)

            customViewBinding.actionPositive.setOnClickListener {
                bottomSheetDialog.cancel()
                onDone(request)
            }

            customViewBinding.actionNegative.setOnClickListener {
                bottomSheetDialog.cancel()
            }

        }
    }

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentAllRequestsBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            binding.recyclerView.adapter = adapter
            binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
            binding.recyclerView.addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            binding.recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(
                binding.recyclerView.layoutManager as LinearLayoutManager
            ) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    fetchLatestRequests(latestPagingBundle!!.nextPage)
                }
            })
            binding.swipeRefreshLayout.setOnRefreshListener {
                adapter.clear()
                latestPagingBundle = null
                fetchLatestRequests(0)
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {
        fetchLatestRequests()
    }

    override fun provideScreenTitle() = getString(R.string.all_open_requests)

    override fun provideTag() = TAG

    private fun fetchLatestRequests(page: Int = 0) {
        scope.launch {
            binding.swipeRefreshLayout.isRefreshing = true
            latestPagingBundle = requestRepo.getAllOpenRequests(page, sessionRepo.obtainUserId())
            adapter.submitList(
                adapter.currentList + latestPagingBundle!!.list.toMutableList()
            ) {
                toggleUiVisibility()
            }
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun toggleUiVisibility() {
        binding.recyclerView.show(adapter.currentList.isNotEmpty())
        binding.viewEmptyList.root.show(adapter.currentList.isEmpty())
        binding.viewEmptyList.text.text = Html.fromHtml(getString(R.string.no_open_requests))
        binding.viewEmptyList.text.setLinkTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.indigo
            )
        )
        binding.viewEmptyList.text.setOnClickListenerVisible {
            baseActivity.screenNavigator().toMyRequestsScreen()
        }
    }

    companion object {
        const val TAG = "AllRequestsFragment"
    }
}

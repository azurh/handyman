package ba.fsre.handyman.android.activities

import android.app.Activity
import ba.fsre.handyman.android.ScreenNavigator
import ba.fsre.handyman.android.fragments.BaseFragment

interface BaseActivity {
    fun addFragment(baseFragment: BaseFragment<*>)
    fun screenNavigator(): ScreenNavigator
    fun context(): Activity
    fun close()
}

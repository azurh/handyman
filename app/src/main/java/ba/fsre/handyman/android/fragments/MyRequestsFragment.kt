package ba.fsre.handyman.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import ba.fsre.handyman.android.R
import ba.fsre.handyman.android.adapters.RequestsAdapter
import ba.fsre.handyman.android.databinding.FragmentMyRequestsBinding
import ba.fsre.handyman.android.utils.*
import ba.fsre.handyman.domain.model.PagingBundle
import ba.fsre.handyman.domain.model.Request
import ba.fsre.handyman.domain.repository.RequestRepository
import ba.fsre.handyman.domain.repository.SessionRepository
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class MyRequestsFragment : BaseFragment<FragmentMyRequestsBinding>() {

    private val requestRepo: RequestRepository by inject()
    private val sessionRepo: SessionRepository by inject()
    private var latestPagingBundle: PagingBundle<Request>? = null

    private val adapter = RequestsAdapter(
        RequestsAdapter.Type.MY_REQUESTS,
        object : RequestsAdapter.RequestInteractionListener {
            override fun onAccept(request: Request) {}
            override fun onComplete(request: Request) {
                if (request.servicerId == null) {
                    val dialog = MaterialDialog(requireContext()).apply {
                        title(res = R.string.app_name)
                        message(res = R.string.mark_as_completed_no_servicer)
                        positiveButton(res = R.string.ok)
                    }
                    dialog.show()
                    return
                }
                showCompletePrompt(request) {
                    scope.launch {
                        try {
                            requestRepo.markAsCompleted(
                                requestId = request.id!!,
                                userId = sessionRepo.obtainUserId()!!
                            )
                            notifyChanged(request)
                        } catch (e: Exception) {
                            snack(e.message!!)
                        }
                    }
                }
            }

            override fun onMyJob(request: Request) {}
        })

    private fun notifyChanged(request: Request) {
        val index = adapter.currentList.indexOf(request)
        request.completed = true
        adapter.notifyItemChanged(index)
    }

    private fun showCompletePrompt(request: Request, onDone: (Request) -> Unit) {
        val dialog = MaterialDialog(requireContext()).apply {
            title(res = R.string.mark_as_completed_title)
            positiveButton(res = R.string.mark_complete) {
                onDone(request)
            }
            negativeButton(res = R.string.no) {}
        }
        dialog.show()
    }

    override fun bindView(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentMyRequestsBinding.inflate(inflater, container, false)
    }

    override fun setViewBehaviour(isCached: Boolean, savedInstanceState: Bundle?) {
        if (!isCached) {
            binding.recyclerView.adapter = adapter
            binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
            binding.recyclerView.addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            binding.recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(
                binding.recyclerView.layoutManager as LinearLayoutManager
            ) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    if (latestPagingBundle?.totalDocs ?: Integer.MAX_VALUE < adapter.currentList.size)
                        fetchLatestRequests(latestPagingBundle!!.nextPage)
                }
            })
            binding.swipeRefreshLayout.setOnRefreshListener {
                adapter.clear()
                latestPagingBundle = null
                fetchLatestRequests(0)
            }
            binding.fab.setOnClickListener {
                baseActivity.screenNavigator().toNewRequestScreen()
            }
        }
    }

    override fun onReady(savedInstanceState: Bundle?) {}

    override fun onStart() {
        super.onStart()
        adapter.clear()
        fetchLatestRequests(0)
    }

    override fun provideScreenTitle() = getString(R.string.my_services)

    override fun provideTag() = TAG

    private fun fetchLatestRequests(page: Int = 0) {
        scope.launch {
            binding.swipeRefreshLayout.isRefreshing = true
            latestPagingBundle = requestRepo.getByUser(sessionRepo.obtainUserId()!!)
            adapter.submitList(
                adapter.currentList + latestPagingBundle!!.list.toMutableList()
            ) { toggleUiVisibility() }
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun toggleUiVisibility() {
        binding.recyclerView.show(adapter.currentList.isNotEmpty())
        binding.viewEmptyList.root.show(adapter.currentList.isEmpty())
        binding.viewEmptyList.text.setText(R.string.no_my_requests)
        binding.viewEmptyList.text.setLinkTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.indigo
            )
        )
        binding.viewEmptyList.text.setOnClickListenerVisible {
            baseActivity.screenNavigator().toMyRequestsScreen()
        }
    }

    companion object {
        const val TAG = "MyRequestsFragment"
    }
}

package ba.fsre.handyman.android.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ba.fsre.handyman.android.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //  setContentView(R.layout.activity_login)
    }
}